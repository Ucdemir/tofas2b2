//
//  OparViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 25/06/2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift
import FOView
import ImageSlideshow
import DropDown
import Kingfisher
class OparViewController:UIViewController, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
  
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    let loaderAlert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    
   
    @IBOutlet weak var productInfoView: ProductInfoView!
    @IBOutlet weak var productsLabel: UILabel!
    var detayliIncele : UIButton!
    var announcements : [Announcement]?
    var products : [Announcement]?
    var pointsTableExpanded = false
    var encTableExpanded = false
    static var askedBayi = false
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var tesfikTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pointsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var headerBoardingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productsBoardingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var campaignHeight: NSLayoutConstraint!
    
    @IBOutlet weak var oneCikanHeight: NSLayoutConstraint!
    
 
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollviewHeight: NSLayoutConstraint!
    
    let pointsTableHeaderFont=UIFont(name: "HelveticaNeue-Medium", size:10);
    let encTableHeaderFont=UIFont(name: "HelveticaNeue-Light", size:13);
    let encTableBoldHeaderFont=UIFont(name: "HelveticaNeue-Bold", size:13);
    let boldFont=UIFont(name: "HelveticaNeue-Bold", size:12);
    @IBOutlet weak var pointsTableView: UITableView!
    @IBOutlet weak var encTableView: UITableView!
    
    //BYZ
    @IBOutlet weak var oneCikanCollectionView: UICollectionView!
   
    @IBOutlet weak var campaignTableView: UITableView!
    
    var arrOneCikan = [ModelOneCikan]()
    
    @IBOutlet weak var onboardingView: ImageSlideshow!
    @IBOutlet weak var productsBoardingView: ImageSlideshow!
  //  @IBOutlet weak var dropView: UIView!
    //let dropDown = DropDown()
    var amontCart:UILabel!

    let imageViewArray2 : [UIImage]? = [#imageLiteral(resourceName: "kampanya_1")   ,#imageLiteral(resourceName: "kampanya_1"),#imageLiteral(resourceName: "kampanya_1")]
    var appNotificationBarButton: MIBadgeButton!      // Make it global
    var repo:HomepageRepository?
    
    var pointsTableData=[PointTableData]()
    var tesfikTableData=[TesfikTableData]()
    var dataCampaign = [ModelCampaign]()

   func RecalculateHeights(){
        var pointsCount:Int!
        var encCount:Int!
        if(self.pointsTableExpanded){
            pointsCount = self.pointsTableData.count+2
        }
        else{
            pointsCount = 2
         }
        
        if(self.encTableExpanded){
            encCount = self.tesfikTableData.count+1
        }
        else{
            encCount = 2
        }
        let headerBoardHeight=self.view.frame.height/4
        
        let pointsTableHeight=pointsCount*40+40
        let tesfikTableHeight=encCount*40+50
        let campaignTableHeight = dataCampaign.count*220+50
        
        
        
       // headerBoardingViewHeight.constant = view.frame.width/2
        //BYZ
       // productsBoardingViewHeight.constant = view.frame.width
        pointsTableViewHeight.constant = CGFloat(integerLiteral: pointsTableHeight)
        tesfikTableViewHeight.constant = CGFloat(integerLiteral: tesfikTableHeight)
        campaignHeight.constant = CGFloat(integerLiteral: campaignTableHeight)
        oneCikanHeight.constant = CGFloat(integerLiteral: 120)
    
        //BYZ
    let totalHeight = headerBoardHeight+pointsTableViewHeight.constant+tesfikTableViewHeight.constant + campaignHeight.constant +
        oneCikanHeight.constant
        scrollviewHeight.constant = totalHeight+70+70+70
       viewHeight.constant = totalHeight+110+70+70
        
        
        view.layoutIfNeeded()
    }
    @IBAction func ExpandEncTable(_ sender: Any) {
        
        self.encTableExpanded = !encTableExpanded
        RecalculateHeights()

        self.encTableView.reloadData()
    }
    @IBAction func ExpandPointsTable(_ sender: Any) {
        self.pointsTableExpanded = !pointsTableExpanded
        RecalculateHeights()

        self.pointsTableView.reloadData()
    }
    
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        print("current page:", page)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        repo = HomepageRepository()
        
        repo?.getCampaign(completionHandler:{ response in
              self.dataCampaign = response.data
                        DispatchQueue.main.async {
                            self.campaignTableView.reloadData()
                            self.activityIndicator.stopAnimating()

                        }
             print (1)
                    })
        
 
        
        //ColectionViewCEll
        self.oneCikanCollectionView.dataSource = self
        self.oneCikanCollectionView.delegate = self
        self.oneCikanCollectionView.register(UINib(nibName: "OneCikanCV", bundle: .main), forCellWithReuseIdentifier: "CVOneCikan")
        self.campaignTableView.register(UINib(nibName: "CampaignTableView", bundle: .main), forCellReuseIdentifier: "CampaignTVC")
        registerHeader()
        self.campaignTableView.dataSource = self
        self.campaignTableView.delegate = self
         
        
       
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        self.oneCikanCollectionView.setCollectionViewLayout(layout, animated: true)
        
        view.addSubview(activityIndicator)
        activityIndicator.frame = UIScreen.main.bounds
        activityIndicator.hidesWhenStopped = true
        _ = navigationController!.view.snapshotView(afterScreenUpdates: true)

        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = "Opar Web"
    
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        initTables()
        loadFooView()
 
        
        
        //BYZ
        /*productsBoardingView.currentPageChanged = { page in
            // do whatever you want eg:
           print(self.products![page].Price)
            self.productInfoView.nameLabel?.text = self.products![page].Header
            self.productInfoView.codeLabel?.text = self.products![page].Code
            self.productInfoView.priceLabel?.text = self.products![page].Price+"TL + KDV"

        }*/
        detayliIncele = UIButton(frame: CGRect(x: onboardingView.frame.width-150, y: onboardingView.frame.height-100, width: 100, height: 25))
        detayliIncele.titleLabel!.textAlignment = .center //For center alignment
        detayliIncele.setTitle("DETAYLI İNCELE", for: .normal)
        detayliIncele.titleLabel!.textColor = .white
        detayliIncele.backgroundColor = .lightGray//If required
        detayliIncele.titleLabel!.font = UIFont.systemFont(ofSize: 13)
        detayliIncele.alpha = 0.8
        
        detayliIncele.addTarget(self, action: #selector(OparViewController.didTap), for: .touchUpInside)

          onboardingView.addSubview(detayliIncele)
        let btn1 = UIButton(type: .custom)
        
        btn1.setTintWithImage(imageName: "basketicon", tintColor: UIColor.white)
        //btn1.setImage(UIImage(named: ), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        
        btn1.showsTouchWhenHighlighted  = true
        self.appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        
        //self.appNotificationBarButton.setImage(UIImage(named: "basketicon"), for: .normal)
        self.appNotificationBarButton.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        
        self.appNotificationBarButton.setTintWithImage(imageName: "basketicon", tintColor: UIColor.white)

        self.appNotificationBarButton.addTarget(self, action: #selector(onShopButton(_:)), for: .touchUpInside)
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: self.appNotificationBarButton)


        let btn2 = UIButton(type: .custom)
        btn2.tintColor = UIColor.white
        
        btn2.setTintWithImage(imageName: "search", tintColor: UIColor.white)
 
        
        
        
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)

        self.navigationItem.setRightBarButtonItems([item2,App_NotificationBarButton], animated: true)

        if(OparViewController.askedBayi == false){
            showPopup()
            OparViewController.askedBayi = true
        }
        if(appNotificationBarButton != nil){
            var basketRepo = BasketRepository()
            basketRepo.GetItemCount(completionHandler:{ response in
                self.appNotificationBarButton.badgeString = String(response)
            })
            
        }

    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOneCikan.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CVOneCikan = collectionView.dequeueReusableCell(withReuseIdentifier: "CVOneCikan", for: indexPath) as! CVOneCikan
        
        var data = arrOneCikan[indexPath.row]

        
        let url = URL(string: data.img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

      
        
        cell.productImg.kf.setImage(with: url)
        
        
        
        cell.lblProductId.text = data.productCode
        
        
        var uilabel : String!
        
        

        cell.lblProductPrice.text = "\(data.price) TL + KDV"
      
      
        cell.productName.text = data.productName
        
        
          return cell
      }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)//here your custom value for spacing
      }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let lay = collectionViewLayout as! UICollectionViewFlowLayout
             /* let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing

        return CGSize(width:widthPerItem, height:160)*/
    
     return CGSize(width: 160, height: collectionView.frame.height)
  }
    
    override func viewWillAppear(_ animated: Bool) {
        if(appNotificationBarButton != nil){
            var basketRepo = BasketRepository()
            basketRepo.GetItemCount(completionHandler:{ response in
                self.appNotificationBarButton.badgeString = String(response)
            })

        }

    }
    func changeBayi(){
        showPopup()

    }
    

    @IBAction func openSearch(_ sender: Any) {
        performSegue(withIdentifier: "oparSearchSegue", sender: self)
        
    }
    
     func openSearch() {
        performSegue(withIdentifier: "oparSearchSegue", sender: self)
        
    }
    
    func quickOrder(){
        performSegue(withIdentifier: "oparQuickOrderSegue", sender: self)
    }

    func setPopularLisT(){
        repo?.GetPopularList(completionHandler:{ response in
            
            print("foo response")
            var imageViewArray = [ImageSource]()
            
            for  row in response{
                
                
                var data = ModelOneCikan(img: row.Path, productCode: row.Code, productName: row.Header, price: row.Price)
                
                
                self.arrOneCikan.append(data)
               
                
                
               /* let url = URL(string: announcement.Path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                if(data == nil){
                    continue
                }
                var image = UIImage(data: data!)!
                let myInsets : UIEdgeInsets = UIEdgeInsets(top: 50, left: 50, bottom: 40, right: 00)
                image.resizableImage(withCapInsets: myInsets)
                imageViewArray.append(ImageSource(image:image))*/
            }
            
            
            self.oneCikanCollectionView.reloadData()
            
            // self.announcements = response
          //  self.productsBoardingView.setImageInputs(imageViewArray)
            //self.products = response

            self.reloadInputViews()
            
        })
    }
    func showPopup() {

        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bayiSelectionViewController") as! BayiSelectionViewController
        popOverVC.vc = self
       // popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.addChild(popOverVC)
        popOverVC.view.frame = view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
 
    }
    @IBAction func onShopButton(_ sender: Any) {
        performSegue(withIdentifier: "oparSepetimSegue", sender: self)
    }
    @IBAction func openMenu(_ sender: Any) {
        self.presentLeftMenuViewController(self)
    }
    func SetNavTitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "OparWeb"
        }
        else{
            label.text = "OparWeb\n(\(SessionInformation.Bayi_?.name ?? ""))"
        }
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
        
    }
    func onFirsatKosesi() {
        performSegue(withIdentifier: "oparFirsatKosesiSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "oparFirsatKosesiSegue" {
            if let toViewController = segue.destination as? SearchRestrictionViewController {
                toViewController.firsatKosesi = true
            }
        } }
    func initTables(){
        pointsTableView.backgroundColor = UIColor.clear
        encTableView.backgroundColor = UIColor.clear
        
        campaignTableView.backgroundColor = UIColor.clear
    
      // encTableView.separatorStyle = .none
      //  pointsTableView.separatorStyle = .none
        
        encTableView.tableFooterView =  UIView(frame: .zero)
        pointsTableView.tableFooterView =  UIView(frame: .zero)
        campaignTableView.tableFooterView = UIView(frame: .zero)
        
        
        pointsTableView.dataSource = self
        encTableView.dataSource = self
       // campaignTableView.dataSource = self
        repo?.GetPointList(completionHandler:{ response in
            self.pointsTableData = response
            DispatchQueue.main.async {
                self.pointsTableView.reloadData()
                self.RecalculateHeights()
            }
        })
        
        repo?.GetTesfikList(completionHandler:{ response in
            self.tesfikTableData = response
            DispatchQueue.main.async {
                self.encTableView.reloadData()
                
                self.RecalculateHeights()
            }
        })
        
        repo?.getCampaign(completionHandler: {response in
            self.dataCampaign = response.data
           DispatchQueue.main.async {
                self.campaignTableView.reloadData()
                
                self.RecalculateHeights()
            }
            
        })

    }
 
    @objc func didTap() {
        print(onboardingView.currentPage)
        if(self.announcements == nil || self.announcements!.count
            <= onboardingView.currentPage){
            return
        }
        var announcement = self.announcements![onboardingView.currentPage]
        showProduct(product: announcement)
    }
    func showProduct( product:Announcement){
        let presentedViewController = self.storyboard?.instantiateViewController(withIdentifier: "OparAnnouncementVC") as! OparAnnouncementViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        
        let url = URL(string: product.Path)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        presentedViewController.imageView.image = UIImage(data: data!)
        presentedViewController.textView.text = product.Header
        self.present(presentedViewController, animated: true, completion: nil)
        
    }
    func loadFooView(){
        self.activityIndicator.startAnimating()

        self.onboardingView.backgroundColor = UIColor.clear
        self.onboardingView.contentScaleMode = .scaleToFill
        self.onboardingView.slideshowInterval = 3.0
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OparViewController.didTap))
        self.onboardingView.addGestureRecognizer(gestureRecognizer)
    
        
        //BYZ
        //CollectiViewCEll yaparken silindi
        /*self.productsBoardingView.backgroundColor = UIColor.clear
        self.productsBoardingView.contentScaleMode = .scaleToFill
        self.productsBoardingView.slideshowInterval = 3.0*/
   
        var loaded1 = false
        var loaded2 = false
        
        repo?.GetPopularList(completionHandler:{ response in
            
            print("foo response")
            var imageViewArray = [ImageSource]()
            
            for  announcement in response{
                
                let url = URL(string: announcement.Path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                if(data == nil){
                    continue
                }
                var image = UIImage(data: data!)!
                let myInsets : UIEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                image.resizableImage(withCapInsets: myInsets)
                imageViewArray.append(ImageSource(image:image))
            }
            
            self.products = response
            //BYZ
           // self.productsBoardingView.setImageInputs(imageViewArray)
            loaded1 = true
            
            if(loaded1 && loaded2){
                self.activityIndicator.stopAnimating()

            }

            
        })
        repo?.GetAnnouncementList(completionHandler:{ response in

            print("foo response")
            var imageViewArray = [ImageSource]()

            for  announcement in response{


                let url = URL(string: announcement.Path)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                if(data == nil){
                    continue
                }
                var image = UIImage(data: data!)!
                let myInsets : UIEdgeInsets = UIEdgeInsets(top: 10, left: 70, bottom: 10, right: 10)
                image.resizableImage(withCapInsets: myInsets)
                imageViewArray.append(ImageSource(image:image))
            }
            
            self.announcements = response
            self.onboardingView.setImageInputs(imageViewArray)
            loaded2 = true
            if(loaded1 && loaded2){
                self.activityIndicator.stopAnimating()
                
            }
            
           

        })
       
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        RecalculateHeights()
        SetNavTitle()
        //self.encTableHeight?.constant = self.encTableView.contentSize.height
        //self.pointsTableHeight?.constant = self.pointsTableView.contentSize.height
    }

 func registerHeader() {
     let nib = UINib(nibName: "CampaignTVCHeader", bundle: nil)
    self.campaignTableView.register(nib, forHeaderFooterViewReuseIdentifier: "CampaignTVCHeader")
 }
}

extension OparViewController: FODelegate {
    func FOnboarding(_ foView: FOView, getCountPageControl: Int) {
        print(getCountPageControl)
    }
}
extension OparViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView==self.pointsTableView){
            
            if(!pointsTableExpanded){
                
                if(indexPath.row==0)
                {
                    
                    let cell:PointsTableViewCell = self.pointsTableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell") as! PointsTableViewCell
                    
                    cell.setFont(font: pointsTableHeaderFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                    
                    
                    cell.backgroundColor=encTableView.backgroundColor
                    cell.points.text="PUAN   "
                    cell.left.text="KAZANIM"
                    cell.usage.text="KULLANIM"
                    cell.win.text="KALAN"
                    cell.points.font = boldFont
                    cell.left.font = boldFont
                    cell.usage.font = boldFont
                    cell.win.font = boldFont
                    return cell
                }
                var data = self.pointsTableData
                let cell:PointsTableViewCell = self.pointsTableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                
                
                var kazanilan=0
                var kalan=0
                var harcanan=0
                for data in pointsTableData{
                    kazanilan += data.KazanilanInt
                    harcanan += data.HarcananInt
                    kalan += data.KalanInt
                }
                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.setFont(font: boldFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "Toplam"
                cell.win.text = "\(GetStringFromValueWithoutTL(value: kalan)),00"
                cell.usage.text =  "\(GetStringFromValueWithoutTL(value: harcanan)),00"
                cell.left.text =  "\(GetStringFromValueWithoutTL(value: kazanilan)),00"
                
                
                return cell
                
            }

            if(indexPath.row==0)
            {
                
                 let cell:PointsTableViewCell = self.pointsTableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell") as! PointsTableViewCell
                
                cell.setFont(font: pointsTableHeaderFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                

                cell.backgroundColor=encTableView.backgroundColor
                cell.points.text="PUAN   "
                cell.points.font = boldFont
                cell.left.font = boldFont
                cell.usage.font = boldFont
                cell.win.font = boldFont
                cell.left.text="KAZANIM"
                cell.usage.text="KULLANIM"
                cell.win.text="KALAN"
                return cell
            }
            else if(indexPath.row==self.pointsTableData.count+1){
                var data = self.pointsTableData
                let cell:PointsTableViewCell = self.pointsTableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                
                
                var kazanilan=0
                var kalan=0
                var harcanan=0
                for data in pointsTableData{
                    kazanilan += data.KazanilanInt
                    harcanan += data.HarcananInt
                    kalan += data.KalanInt
                }
                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.setFont(font: boldFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "Toplam"
                cell.win.text = "\(GetStringFromValueWithoutTL(value: kalan)),00"
                cell.usage.text = "\(GetStringFromValueWithoutTL(value: harcanan)),00"
                cell.left.text =  "\(GetStringFromValueWithoutTL(value: kazanilan)),00"


                return cell
                
            }
            else{
                var data = self.pointsTableData[indexPath.row-1]
                let cell:PointsTableViewCell = self.pointsTableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)

                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "\(data.Yil!)"
                cell.win.text = "\(data.Kalan!),00"
                cell.usage.text = "\(data.Harcanan!),00"
                cell.left.text = "\(data.Kazanilan!),00"
                
                return cell
            }
        }
        
        else if (tableView == self.encTableView){
            if(!encTableExpanded ){
                
                if(indexPath.row == 0){
                    
                
                    let cell:PointsTableViewCell = self.encTableView.dequeueReusableCell(withIdentifier: "encTableViewCell") as! PointsTableViewCell
                    cell.setFont(font: pointsTableHeaderFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                    cell.backgroundColor=encTableView.backgroundColor
                    
                    cell.points.text="Hedef Dönem"
                    cell.left.text="Hak Ediş \nKalan Tutar(TL)"
                    cell.left.numberOfLines=2
                    cell.usage.text="Gerçekleşen\n  Tutar (TL)"
                    cell.usage.numberOfLines=2
                    
                    cell.points.font = boldFont
                    cell.left.font = boldFont
                    cell.usage.font = boldFont
                    cell.win.font = boldFont
                    
                    cell.usage.textAlignment = .left
                    cell.win.text="Hedef\n   Tutar(TL)"
                    cell.win.textAlignment = .center
                    cell.left.numberOfLines=0
                    cell.win.numberOfLines=0
                    
                  
                    
                    return cell
                
                return cell
                }
                let cell:PointsTableViewCell = self.encTableView.dequeueReusableCell(withIdentifier: "encTableViewCell") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                
                
                var hedeflenen=0
                var gerceklesen=0
                var hediye=0
                for data in tesfikTableData{
                    gerceklesen += data.gerceklesenInt
                    hedeflenen += data.hedeflenenInt
                    hediye += data.kalanInt
                }
                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.setFont(font: boldFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "YILLIK"
                cell.win.text = "\(GetStringFromValueWithoutTL(value: hedeflenen)),00"
                cell.usage.text =  "\(GetStringFromValueWithoutTL(value: gerceklesen)),00"
                cell.left.text =  "\(GetStringFromValueWithoutTL(value: hediye)),00"
                //   cell.win.textAlignment = .left
                // cell.usage.textAlignment = .center
                
                
                
                
                return cell
            }
            if( indexPath.row==self.tesfikTableData.count+1 ){
                
                if(indexPath.row==0)
                {
                    
                    let cell:PointsTableViewCell = self.encTableView.dequeueReusableCell(withIdentifier: "encTableViewCell") as! PointsTableViewCell
                    cell.setFont(font: pointsTableHeaderFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                    cell.backgroundColor=encTableView.backgroundColor
                    
                    cell.points.text="Hedef Dönem"
                    cell.left.text="Hediye Hak Ediş \nKalan Tutar(TL)"
                    cell.left.numberOfLines=2
                    cell.usage.text="Gerçekleşen\n  Tutar (TL)"
                    cell.usage.numberOfLines=2
                    
                    cell.usage.textAlignment = .left
                    cell.win.text="Hedef\n   Tutar(TL)"
                    cell.win.textAlignment = .center
                    cell.left.numberOfLines=0
                    cell.win.numberOfLines=0
                    
                    cell.points.font = boldFont
                    cell.left.font = boldFont
                    cell.usage.font = boldFont
                    cell.win.font = boldFont
                    
                    return cell
                    
                }
                let cell:PointsTableViewCell = self.encTableView.dequeueReusableCell(withIdentifier: "encTableViewCell") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                
                
                var hedeflenen=0
                var gerceklesen=0
                var hediye=0
                for data in tesfikTableData{
                    gerceklesen += data.gerceklesenInt
                    hedeflenen += data.hedeflenenInt
                    hediye += data.kalanInt
                }
                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.setFont(font: boldFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "YILLIK"
                cell.win.text = ""+GetStringFromValueWithoutTL(value: hedeflenen)+",00"
                cell.usage.text =  ""+GetStringFromValueWithoutTL(value: gerceklesen)+",00"
                cell.left.text = ""+GetStringFromValueWithoutTL(value: hediye)+",00"
             //   cell.win.textAlignment = .left
               // cell.usage.textAlignment = .center
             
                
                
                
                return cell
                
            }
            if(indexPath.row==0)
            {
                
                let cell:PointsTableViewCell = self.encTableView.dequeueReusableCell(withIdentifier: "encTableViewCell") as! PointsTableViewCell
                cell.setFont(font: pointsTableHeaderFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                cell.backgroundColor=encTableView.backgroundColor

                cell.points.text="Hedef Dönem"
                cell.left.text="Hakediş \nKalan Tutar(TL)"
                cell.left.numberOfLines=2
                cell.usage.text="Gerçekleşen\n  Tutar (TL)"
                cell.usage.numberOfLines=2
                
                cell.points.font = boldFont
                cell.left.font = boldFont
                cell.usage.font = boldFont
                cell.win.font = boldFont

                cell.usage.textAlignment = .left
                cell.win.text="Hedef\n   Tutar(TL)"
                cell.win.textAlignment = .center
                cell.left.numberOfLines=0
                cell.win.numberOfLines=0

                return cell
                
            }
   

            else{
                var data = self.tesfikTableData[indexPath.row-1]

                let cell:PointsTableViewCell = self.encTableView.dequeueReusableCell(withIdentifier: "encTableViewCell") as! PointsTableViewCell
                
                //MAKE CELLS OPAQUE

                cell.backgroundColor = UIColor.clear

                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                
                cell.points.text = data.Yil
                cell.left.text = ""+data.Kalan+",00"
                cell.usage.text = ""+data.Gerceklesen+",00"
                cell.win.text =   ""+data.Hedeflenen+",00"

                if(indexPath.row == 3 || indexPath.row == 6){
                    cell.layer.masksToBounds = true
                    cell.layer.cornerRadius = 3
                    cell.layer.borderWidth = 1
                    cell.layer.shadowOffset = CGSize(width: -1, height: 1)
                    cell.layer.borderColor = UIColor.black.cgColor
                    cell.setFont(font: encTableBoldHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)

                }
                else{
                    cell.layer.cornerRadius = 0
                    cell.layer.borderWidth = 0
                    cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                    cell.backgroundColor = UIColor.clear
                    
                }
                return cell
            }
           
        }else{
            let cell:CampaignTVC = self.campaignTableView.dequeueReusableCell(withIdentifier: "CampaignTVC") as! CampaignTVC
            cell.backgroundColor=encTableView.backgroundColor
            let modelCampaign = dataCampaign[indexPath.row]
            
            cell.lblCampaignName.text = modelCampaign.definition
            cell.lblGecerlilikTarihi.text = modelCampaign.startDate
            cell.lblHedef.text = String(modelCampaign.defaultTarget)
            //gerçeklkeşen //cell.lblGerçeklesen.text = modelCampaign.g
            //hedef gerçekleşen
            //hake diş gerçeleşen
            cell.lblExplain.text = modelCampaign.explanation
            
            
            return cell
        }
    }
    func GetStringFromValue(value:Int) ->String{
        var result=""
        var strValue=String(value).reversed()
        var counter=0
        for char in strValue {
            result.insert(char, at: result.startIndex)
            counter = counter+1
            if(counter%3 == 0 && strValue.count != counter){
                result.insert(".", at: result.startIndex)
            }
        }
        
        return result + ",00TL"
    }
    func GetStringFromValueWithoutTL(value:Int) ->String{
        var result=""
        var strValue=String(value).reversed()
        var counter=0
        for char in strValue {
            result.insert(char, at: result.startIndex)
            counter = counter+1
            if(counter%3 == 0 && strValue.count != counter){
                result.insert(".", at: result.startIndex)
            }
        }
        
        return result
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(tableView == self.campaignTableView ){
            return 220

        }
        else  if ( indexPath.row == 0){
       
        return 50
        
        }else{
        return 30.0;
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView==self.pointsTableView){
            if(self.pointsTableExpanded == false){
                return 2
            }
            return self.pointsTableData.count+2
            
        }
        else  if(tableView==self.encTableView){
            if(self.encTableExpanded == false){
                return 2
            }
            return self.tesfikTableData.count+2
            
        }else{
            
            return dataCampaign.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(tableView == self.pointsTableView){
            
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! CustomHeaderCell
           
            headerCell.label.textColor = UIColor.black
            headerCell.label.text = "Web Puan"

            if(self.pointsTableExpanded){
                headerCell.button.setImage(UIImage(named: "up"), for: .normal)
            }
            else{
                headerCell.button.setImage(UIImage(named: "down"), for: .normal)

            }

            return headerCell

        }
        else if (tableView == self.encTableView){
            let  headerCell = self.encTableView.dequeueReusableCell(withIdentifier: "HeaderCell2") as! CustomHeaderCell
          
            headerCell.label.textColor = UIColor.black
            headerCell.label.text = "OparWeb Teşvik"
            headerCell.button.tintColor = UIColor.lightGray

            if(self.encTableExpanded){
                headerCell.button.setImage(UIImage(named: "up"), for: .normal)
            }
            else{
                headerCell.button.setImage(UIImage(named: "down"), for: .normal)
                
            }


            return headerCell
        }else{
            let headerCell = self.campaignTableView.dequeueReusableHeaderFooterView(withIdentifier: "CampaignTVCHeader") as! CampaignTVCHeader

            return headerCell
        }

    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(tableView==pointsTableView){
            return "Web Puan"
        }else if tableView == encTableView{
            return "OPAR TEŞVİK TABLOSU"
        }else{
            return nil
        }
    }

      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == self.campaignTableView {
            
            return 70
        }else{
            return 40
        }
        
    }
 
  
}


extension OparViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!)!
        
        self.pointsTableExpanded = !self.pointsTableExpanded
        self.pointsTableView.reloadData()
        
    }
}
