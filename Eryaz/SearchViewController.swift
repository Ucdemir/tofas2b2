//
//  SearchViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class SearchViewController : UIViewController{
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 60);
    }
    
}


extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
                
                let cell:SearchTableCell = self.tableView.dequeueReusableCell(withIdentifier: "searchTableCell") as! SearchTableCell
        
            cell.idLabel.textColor = DesignData.darkGrey
                cell.nameLabel.textColor = DesignData.darkGrey
                cell.priceLabel.textColor = DesignData.darkGrey
                cell.idLabel.text = "5132190"
                cell.nameLabel.text = " FULL PAKET KROM 4PRC LN>07"
                cell.priceLabel.text = "793.50"
                return cell
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       
        return 100.0;//Choose your custom row height
    }
    
}

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      

    }
}
