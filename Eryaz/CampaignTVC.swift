//
//  CampaignTVC.swift
//  Eryaz
//
//  Created by Omer Demirci on 13.06.2020.
//  Copyright © 2020 EryazSoftware. All rights reserved.
//

import UIKit

class CampaignTVC: UITableViewCell {
    
    
    
    @IBOutlet weak var lblCampaignName: UILabel!
    @IBOutlet weak var lblGecerlilikTarihi: UILabel!
    @IBOutlet weak var lblHedef: UILabel!
    @IBOutlet weak var lblGerçeklesen: UILabel!
    @IBOutlet weak var lblHedefGegrceklesen: UILabel!
    
    @IBOutlet weak var lblHakEdisIcınKalan: UILabel!
    
    @IBOutlet weak var lblExplain: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
