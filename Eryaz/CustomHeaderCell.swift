//
//  CustomHeaderCellTableViewCell.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 10.03.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit

class CustomHeaderCell: UITableViewCell {
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
