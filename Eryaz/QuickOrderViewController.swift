//
//  QuickOrderViewController.swift
//  Eryaz
//
//  Created by Arda Yucel on 5.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift
class QuickOrderController:UIViewController, UITextFieldDelegate{
    var appNotificationBarButton: MIBadgeButton!      // Make it global

    @IBOutlet weak var parcaKodu1: UITextField!
        @IBOutlet weak var parcaKodu2: UITextField!
        @IBOutlet weak var parcaKodu3: UITextField!
        @IBOutlet weak var parcaKodu4: UITextField!
        @IBOutlet weak var parcaKodu5: UITextField!
    @IBOutlet weak var miktar1: UITextField!
        @IBOutlet weak var miktar2: UITextField!
        @IBOutlet weak var miktar3: UITextField!
        @IBOutlet weak var miktar4: UITextField!
        @IBOutlet weak var miktar5: UITextField!
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var parcaKoduLabel :UILabel!
    @IBOutlet weak var miktarLabel:UILabel!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func SetNavTitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "HIZLI SİPARİŞ"
        }
        else{
            label.text = "HIZLI SİPARİŞ\n(\(SessionInformation.Bayi_?.name ?? ""))"
        }
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
        
    }
    @objc func dismissKeyboard() -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        guard NSCharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        
        return true
    }


    override func viewDidLoad() {
        button.backgroundColor = Color.red
        parcaKoduLabel.backgroundColor = Color.darkBlue
        miktarLabel.backgroundColor = Color.darkBlue
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
 
        
        miktar1.delegate = self
        miktar2.delegate = self
        miktar3.delegate = self
        miktar4.delegate = self
        miktar5.delegate = self

        parcaKodu1.delegate = self
        parcaKodu2.delegate = self
        parcaKodu3.delegate = self
        parcaKodu4.delegate = self
        parcaKodu5.delegate = self

        SetBarButtonItems()
        
        
        miktar1.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        miktar2.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        miktar3.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        miktar4.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        miktar5.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
       
        parcaKodu1.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        parcaKodu2.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        parcaKodu3.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        parcaKodu4.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        parcaKodu5.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)

        
        

    }
    override func viewWillAppear(_ animated: Bool) {
        SetNavTitle()
        if(appNotificationBarButton != nil){
            var basketRepo = BasketRepository()
            basketRepo.GetItemCount(completionHandler:{ response in
                self.appNotificationBarButton.badgeString = String(response)
            })
            
        }    }
    func SetBarButtonItems(){
        
        let btn3 = UIButton(type: .custom)
        
    
       
        btn3.setTintWithImage(imageName: "menuButton", tintColor: UIColor.white)
        btn3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn3.addTarget(self, action: #selector(onMenuButton), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn3)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
        
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "basketicon"), for: .normal)
        btn1.setTintWithImage(imageName: "basketicon", tintColor: UIColor.white)

        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        
        self.appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        //self.appNotificationBarButton.setImage(UIImage(named: "basketicon"), for: .normal)
        self.appNotificationBarButton.setColorWithImage(imageName: "basketicon", tintColor: UIColor.white)

        
        self.appNotificationBarButton.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        self.appNotificationBarButton.addTarget(self, action: #selector(onShopButton(_:)), for: .touchUpInside)
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: self.appNotificationBarButton)
        
        
        let btn2 = UIButton(type: .custom)
        
        
        btn2.setTintWithImage(imageName: "search", tintColor: UIColor.white)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2,App_NotificationBarButton], animated: true)
        
    }
    @IBAction func onMenuButton(){
        self.presentLeftMenuViewController(self)
        
    }
    @IBAction func openSearch(_ sender: Any) {
        if(SessionInformation.TofasCompany == "OPAR"){
            performSegue(withIdentifier: "quickOparSearch", sender: self)

        }
        else{
            performSegue(withIdentifier: "quickMarelliSearch", sender: self)

        }
        
    }
    @IBAction func onShopButton(_ sender: Any) {
        performSegue(withIdentifier: "quickSepetim", sender: self)
    }
    @IBAction func ekle(_ sender: Any) {
        
        var list =  [[String: AnyObject]]()
        if(parcaKodu1.text != "" && miktar1.text != ""){
            var item = QuickOrderItem()
            item.partCode = parcaKodu1.text!
            item.partQuantity = Int(miktar1.text!)
            list.append(item.asDictionary())
        }
        if(parcaKodu2.text != "" && miktar2.text != ""){
            var item = QuickOrderItem()
            item.partCode = parcaKodu2.text!
            item.partQuantity = Int(miktar2.text!)
            list.append(item.asDictionary())
        }
        if(parcaKodu3.text != "" && miktar3.text != ""){
            var item = QuickOrderItem()
            item.partCode = parcaKodu3.text!
            item.partQuantity = Int(miktar3.text!)
            list.append(item.asDictionary())
        }
        if(parcaKodu4.text != "" && miktar4.text != ""){
            var item = QuickOrderItem()
            item.partCode = parcaKodu4.text!
            item.partQuantity = Int(miktar4.text!)
            list.append(item.asDictionary())
        }
        if(parcaKodu5.text != "" && miktar5.text != ""){
            var item = QuickOrderItem()
            item.partCode = parcaKodu5.text!
            item.partQuantity = Int(miktar5.text!)
            list.append(item.asDictionary())
        }
        
        var repo=BasketRepository()
        repo.QuickOrder(items: list,completionHandler:{ response in
            
            
        self.showBasketMessage(done: response)
        })
    }
    func showBasketMessage(done:Bool){
        
        
        var alert : UIAlertController!
        if(done){
            alert = UIAlertController(title: "Başarılı", message: "Ürün sepete başarıyla eklendi!", preferredStyle: UIAlertController.Style.alert)
        }
        else{
            alert = UIAlertController(title: "GEÇERSİZ", message: "Geçersiz parça kodu girdiniz", preferredStyle: UIAlertController.Style.alert)
        }
        
        alert.addAction(UIAlertAction(title: "DEVAM ET", style: .default, handler: { action in
           
            
        
        }))
        alert.addAction(UIAlertAction(title: "Sepete Git", style: .default, handler: { action in
            self.onShopButton(self)
        
        
        }))
        present(alert, animated: true, completion: nil)
        
    }
    
}
