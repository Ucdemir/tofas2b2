//
//  Product.swift
//  Eryaz
//
//  Created by Arda Yucel on 7.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

class Product {
    
    var IsSelected = true
    var Name:String!
    var Code:String!
    var priceWithVat:String!
    var ProductId:Int!
    
    var listeFiyatiWithVat:Double!
    var listeFiyati:Double!
    var kdvliFiyat:Double!
    var kdvsizFiyat:Double!
    
    var avaibleTofas:Bool!
    var avaibleBayi:Bool!
   
    var priceCurrency:String!
    var quantitiy:Int?
    
    var NewProduct:Bool!
    var DiscountStr:String!
    var campaignOpt:Int!
    
    var totalVat:Double!
    var totalPrice:Double!
    var totalDiscount:Double!

    var totalCost:Double!
    var totalCostWithVat:Double!
    var totalYakitPuan:Double!
    
    var iskontoGrubu:String!
    var stillProduced:Bool!
    var minOrder:Int!
    
    var orderId:Int?
    
}
