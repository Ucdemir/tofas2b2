//
//  ContactTableCell.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 20.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
class ContactTableCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 1
            frame.size.height -= 1 * 2
            super.frame = frame
        }
    }

}
