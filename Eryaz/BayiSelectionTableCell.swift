//
//  SideMenuTextCell.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class BayiSelectionTableCell: UITableViewCell {
    
    @IBOutlet weak var bayiKoduLabel: UILabel!
    @IBOutlet weak var shortNameLabel: UILabel!

    
    @IBOutlet weak var img: UIImageView!
    
    
       override func awakeFromNib() {
              super.awakeFromNib()
             
        img.imageColor(color: UIColor.white)
           
           
          }
}
