//
//  MagnetiViewController.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 13.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift
import FOView
import DropDown
import ImageSlideshow
class MagnetiViewController:UIViewController{
    
    var isTableExpanded = false
    static var askedBayi = false
    var appNotificationBarButton: MIBadgeButton!      // Make it global
    @IBOutlet weak var menuButton: UIBarButtonItem!
    let pointsTableHeaderFont=UIFont(name: "HelveticaNeue-Medium", size:12);
    let encTableHeaderFont=UIFont(name: "HelveticaNeue-Light", size:13);
    let boldFont=UIFont(name: "HelveticaNeue-Bold", size:13);
    var repo = HomepageRepository()

    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reklamView: ImageSlideshow!
    @IBOutlet weak var productsView: ImageSlideshow!
    @IBOutlet weak var oldCampaignView: ImageSlideshow!
    var pointsTableData=[PointTableData]()
    var products:[Announcement]?

    override func viewDidLoad() {
        if(MagnetiViewController.askedBayi == false){
            showPopup()
            MagnetiViewController.askedBayi = true
        }
        tableView.dataSource = self
        tableView.delegate = self
        self.appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        //self.appNotificationBarButton.setImage(UIImage(named: "basketicon"), for: .normal)
        self.appNotificationBarButton.setColorWithImage(imageName: "basketicon", tintColor: UIColor.white)

        
        self.appNotificationBarButton.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        self.appNotificationBarButton.addTarget(self, action: #selector(onShopButton(_:)), for: .touchUpInside)
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: self.appNotificationBarButton)
        
        
        let btn2 = UIButton(type: .custom)
        //btn2.setImage(UIImage(named: "search"), for: .normal)
        btn2.setTintWithImage(imageName: "search", tintColor: UIColor.white)

        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(onSearchButton(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2,App_NotificationBarButton], animated: true)

        let tapOnProduct = UITapGestureRecognizer(target: self, action: #selector(productClicked(_:)))
        productsView.addGestureRecognizer(tapOnProduct)
        SetNavTitle()


    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        SetNavTitle()
        //self.encTableHeight?.constant = self.encTableView.contentSize.height
        //self.pointsTableHeight?.constant = self.pointsTableView.contentSize.height
    }

    func bayiSelected(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "MarelliWeb"
        }
        else{
            label.text = "MarelliWeb\n(\(SessionInformation.Bayi_?.code ?? ""))"
        }
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
    }
    func openSepetim(){
        
        performSegue(withIdentifier: "magnetiSepetimSegue", sender: self)
        
    }
    func showProduct( product:Announcement){
        let presentedViewController = self.storyboard?.instantiateViewController(withIdentifier: "MarelliProductViewController") as! MarelliProductViewController
            presentedViewController.providesPresentationContextTransitionStyle = true
            presentedViewController.definesPresentationContext = true
            presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            presentedViewController.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        
            let url = URL(string: product.Path)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            presentedViewController.imageView.image = UIImage(data: data!)
        presentedViewController.textView.text = product.Description
            self.present(presentedViewController, animated: true, completion: nil)
        
    }
    @objc func productClicked(_ sender:UITapGestureRecognizer){
        print(productsView.currentPage)
        if(self.products == nil || productsView.currentPage>=(products?.count)!)
        {
            return
        }
        
        showProduct(product: self.products![productsView.currentPage])
        
    }
    func quickOrder(){
        performSegue(withIdentifier: "marelliQuickOrderSegue", sender: self)
    }
    func showPopup() {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bayiSelectionViewController") as! BayiSelectionViewController
        // popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width
            , height:   view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    override func viewWillAppear(_ animated: Bool) {

        if(appNotificationBarButton != nil){
            var basketRepo = BasketRepository()
            basketRepo.GetItemCount(completionHandler:{ response in
                self.appNotificationBarButton.badgeString = String(response)
            })
            
        }

        setupFooView(completionHandler:{ response in
           
        })
      
        initTable()
        SetNavTitle()
    }
    func SetNavTitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "Marelli Web"
        }
        else{
            label.text = "Marelli Web\n(\(SessionInformation.Bayi_?.name ?? ""))"
        }
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
        
    }
    func changeBayi(){
        showPopup()
        
    }
    func initTable(){
        var repo = HomepageRepository()
        tableView.backgroundColor = UIColor.clear
        tableView.dataSource = self
        repo.GetPointList(completionHandler:{ response in
            self.pointsTableData = response
            DispatchQueue.main.async {
                self.tableView.reloadData()
                if(self.isTableExpanded){
                    self.tableViewHeight.constant = CGFloat((self.pointsTableData.count+2)*50)
                }else{
                   self.tableViewHeight.constant = CGFloat(140)
                }

                self.tableView.reloadData()
                self.view.layoutIfNeeded()

            }
        })
    }
    @IBAction func ExpandTable(){
        self.isTableExpanded =  !self.isTableExpanded
        if(isTableExpanded ){
            self.tableViewHeight.constant = CGFloat((self.pointsTableData.count+2)*50)

        }else{
            self.tableViewHeight.constant = CGFloat(140)

        }
        self.tableView.reloadData()
    }
    func setupFooView(completionHandler: @escaping (Bool) -> Void)  {
   
     
        reklamView.backgroundColor = UIColor.clear
        reklamView.contentScaleMode = .scaleToFill
        reklamView.slideshowInterval = 3.0
        repo.GetAnnouncementList(completionHandler:{ response in
            var imageViewArray = [ImageSource]()
            print("foo response")
            print(response)
            
            for  announcement in response{
                let url = URL(string: announcement.Path)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                imageViewArray.append(ImageSource(image:UIImage(data: data!)!))
            }
            
            self.reklamView.setImageInputs(imageViewArray)
            self.reloadInputViews()
            completionHandler(true)

        })
        
        productsView.backgroundColor = UIColor.clear
        productsView.contentScaleMode = .scaleToFill
        repo.GetMarelliProducts(completionHandler:{ response in
            var imageViewArray = [ImageSource]()
            self.products = response

            print("foo response")
            print(response)
            
            for  announcement in response{
                let path = announcement.Path
                if((path?.isURL())!&&(path?.isImage())!){
                    let url = URL(string:path!)
                    if let data = try? Data(contentsOf: url!) {
                        
                        imageViewArray.append(ImageSource(image:UIImage(data: data)!))
                    }
                }
            }
            
            self.productsView.setImageInputs(imageViewArray)
            self.reloadInputViews()
            completionHandler(true)
            
        })
        oldCampaignView.backgroundColor = UIColor.clear
        oldCampaignView.contentScaleMode = .scaleToFill
        repo.GetOldCampaign(completionHandler:{ response in
            var imageViewArray = [ImageSource]()
            
            print("foo response")
            print(response)
            
            for  announcement in response{
                
                let path = announcement.Path
                if((path?.isURL())!&&(path?.isImage())!){
                    let url = URL(string:path!)
                    if let data = try? Data(contentsOf: url!) {
                        
                        imageViewArray.append(ImageSource(image:UIImage(data: data)!))
                    }
                }
                
            }
            
            self.oldCampaignView.setImageInputs(imageViewArray)
            self.reloadInputViews()
            completionHandler(true)
            
        })
    }
    @IBAction func openMenu(_ sender: Any) {
        self.presentLeftMenuViewController(self)
    }
    @IBAction func onShopButton(_ sender: Any) {
        performSegue(withIdentifier: "magnetiSepetimSegue", sender: self)
    }
    @IBAction func onSearchButton(_ sender: Any) {
        performSegue(withIdentifier: "oparSearchSegue2", sender: self)
    }
    func onFirsatKosesi() {
        performSegue(withIdentifier: "marelliFirsatKosesiSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "marelliFirsatKosesiSegue" {
            if let toViewController = segue.destination as? MarelliSearchRestrictionViewController {
                toViewController.firsatKosesi = true
                
            }
        } }
}

extension MagnetiViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(!self.isTableExpanded){
            
            if(indexPath.row==0)
            {
                
                let cell:PointsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell2") as! PointsTableViewCell
                
                cell.setFont(font: boldFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                
                
                cell.backgroundColor = UIColor.clear
                cell.points.text="PUAN   "
                cell.win.text="KALAN"
                cell.usage.text="KULLANIM"
                cell.left.text="KAZANIM"
                return cell
            }
            var data = self.pointsTableData
            let cell:PointsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell2") as! PointsTableViewCell
            cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
            
            
            var kazanilan=0
            var kalan=0
            var harcanan=0
            for data in pointsTableData{
                kazanilan += data.KazanilanInt
                harcanan += data.HarcananInt
                kalan += data.KalanInt
            }
            //MAKE CELLS OPAQUE
            cell.backgroundColor = UIColor.clear
            
            cell.setFont(font: boldFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
            cell.points.textColor=DesignData.darkGrey
            cell.left.textColor=DesignData.darkGrey
            cell.usage.textColor=DesignData.darkGrey
            cell.win.textColor=DesignData.darkGrey
            cell.points.text = "Toplam"
            cell.points.font = boldFont
            cell.win.text = GetStringFromValue(value: kalan)
            cell.usage.text =  GetStringFromValue(value: harcanan)
            cell.left.text =  GetStringFromValue(value: kazanilan)
            
            
            return cell
            
        }
            
            if(indexPath.row==0)
            {
                
                let cell:PointsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell2") as! PointsTableViewCell
                
                cell.setFont(font: boldFont!,displayP3Red: 45,green: 52,blue: 59,alpha: 0.8)
                
                
                cell.backgroundColor = UIColor.clear
                cell.points.text="PUAN   "
                cell.win.text="KALAN"
                cell.usage.text="KULLANIM"
                cell.left.text="KAZANIM"
                return cell
            }
            else if(indexPath.row==self.pointsTableData.count+1){
                var data = self.pointsTableData
                let cell:PointsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell2") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                
                
                var kazanilan=0
                var kalan=0
                var harcanan=0
                for data in pointsTableData{
                    kazanilan += data.KazanilanInt
                    harcanan += data.HarcananInt
                    kalan += data.KalanInt
                }
                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.setFont(font: boldFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "Toplam"
                cell.win.text = GetStringFromValue(value: kalan)
                cell.usage.text =  GetStringFromValue(value: harcanan)
                cell.left.text =  GetStringFromValue(value: kazanilan)
                
                
                return cell
                
            }
            else{
                var data = self.pointsTableData[indexPath.row-1]
                let cell:PointsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "pointsTableViewCell2") as! PointsTableViewCell
                cell.setFont(font: encTableHeaderFont!,displayP3Red: 132,green: 132,blue: 132,alpha: 1)
                
                //MAKE CELLS OPAQUE
                cell.backgroundColor = UIColor.clear
                
                cell.points.textColor=DesignData.darkGrey
                cell.left.textColor=DesignData.darkGrey
                cell.usage.textColor=DesignData.darkGrey
                cell.win.textColor=DesignData.darkGrey
                cell.points.text = "\(data.Yil!)"
                cell.win.text = "\(data.Kalan!)"
                cell.usage.text = "\(data.Harcanan!)"
                cell.left.text = "\(data.Kazanilan!)"
                
                return cell
            }
        
            
        
    }
    
    func GetStringFromValue(value:Int) ->String{
        var result=""
        var strValue=String(value).reversed()
        var counter=0
        for char in strValue {
            result.insert(char, at: result.startIndex)
            counter = counter+1
            if(counter%3 == 0 && strValue.count != counter){
                result.insert(".", at: result.startIndex)
            }
        }
        
        return result
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row == 0 ){
            return 50
            
        }
        return 30.0;//Choose your custom row height
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.isTableExpanded){
            return self.pointsTableData.count+2
        }
        else{
            return 2
        }
    
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCellMarelli") as! CustomHeaderCell
            headerCell.label.textColor = UIColor.black
            headerCell.label.text = "   WEB PUAN"
            headerCell.label.textAlignment = .left
            
            if(self.isTableExpanded){
                headerCell.button.setImage(UIImage(named: "up"), for: .normal)
            }
            else{
                headerCell.button.setImage(UIImage(named: "down"), for: .normal)
                
            }
            
            
            return headerCell
        
        
    }

    
  
    
    
    
}

extension MagnetiViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!)!
        
        
    }
}
extension String {
    
    public func isImage() -> Bool {
        // Add here your image formats.
        let imageFormats = ["jpg", "jpeg", "png", "gif"]
        
        if let ext = self.getExtension() {
            return imageFormats.contains(ext)
        }
        
        return false
    }
    
    public func getExtension() -> String? {
        let ext = (self as NSString).pathExtension
        
        if ext.isEmpty {
            return nil
        }
        
        return ext
    }
    
    public func isURL() -> Bool {
        return URL(string: self) != nil
    }
    
}
