//
//  PointTableData.swift
//  Eryaz
//
//  Created by Arda Yucel on 6.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

class PointTableData{
    
    var Yil:String!
    var Kazanilan:String!
    var Harcanan:String!
    var Kalan:String!
    var YilInt:Int!
    var KazanilanInt:Int!
    var HarcananInt:Int!
    var KalanInt:Int!
    init(yil:Int,kazanilan:Int,harcanan:Int,kalan:Int){
        Yil = String(yil)
        Kazanilan = GetStringFromValue(value:kazanilan)
        Harcanan = GetStringFromValue(value:harcanan)
        Kalan = GetStringFromValue(value:kalan)
        YilInt = yil
        KazanilanInt = kazanilan
        HarcananInt = harcanan
        KalanInt = kalan
    }
    func GetStringFromValue(value:Int) ->String{
        var result=""
        var strValue=String(value).reversed()
        var counter=0
        for char in strValue {
            result.insert(char, at: result.startIndex)
            counter = counter+1
            if(counter%3 == 0 && strValue.count != counter){
                result.insert(".", at: result.startIndex)
            }
        }
        
        return result + ""
    }
}
