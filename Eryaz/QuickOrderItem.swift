//
//  QuickOrderItem.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 24.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//
class QuickOrderItem{
    
    var partQuantity:Int!
    var partCode:String!
    func asDictionary() -> [String: AnyObject] {
        return ["partQuantity": partQuantity as AnyObject, "partCode": String(partCode) as AnyObject]
    }
}
