//
//  SideMenuTextCell.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class ShoppingCartToplamCell: UITableViewCell {
    @IBOutlet weak var kazanilacakYakitPuanValue: UILabel!
    
    @IBOutlet weak var netTutarValue: UILabel!
    @IBOutlet weak var kdvValue: UILabel!
    @IBOutlet weak var genelToplamValue: UILabel!
    @IBOutlet weak var araTutarValue: UILabel!
    @IBOutlet weak var faturaIskontosuValue: UILabel!
    @IBOutlet weak var satirIskontosuValue: UILabel!
    @IBOutlet weak var toplamTutarValue: UILabel!
    @IBOutlet weak var genelToplam: UILabel!
    @IBOutlet weak var faturaIskontosu: UILabel!
    @IBOutlet weak var satirIskontosu: UILabel!
    @IBOutlet weak var toplamTutar: UILabel!
    
}
