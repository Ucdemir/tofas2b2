//
//  ProductViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import ImageSlideshow
class ProductViewController:UIViewController{
    
    
    @IBOutlet weak var productCodeLabel: UILabel!
    @IBOutlet weak var productTofasStokLabel: UILabel!
    @IBOutlet weak var productIskontoLabel: UILabel!
    @IBOutlet weak var productKdvliListeFiyati: UILabel!
    @IBOutlet weak var productKdvliFiyatLabel: UILabel!
    @IBOutlet weak var productKdvsizFiyatLabel: UILabel!
    @IBOutlet weak var productListeFiyatiLabel: UILabel!
    @IBOutlet weak var listeLabel: UILabel!
    @IBOutlet weak var iskontoLabel: UILabel!
    @IBOutlet weak var kdvsizLabel: UILabel!
    @IBOutlet weak var kdvliLabel: UILabel!
    @IBOutlet weak var bayiStokLabel: UILabel!
    @IBOutlet weak var productTofasStokImage: UIImageView!
    @IBOutlet weak var productBayiStokImage: UIImageView!
    @IBOutlet weak var tofasStokLabel: UILabel!
    @IBOutlet weak var productBayiStokLabel: UILabel!
    @IBOutlet weak var kdvliListeFiyatLabel: UILabel!
    @IBOutlet weak var iskontoGrubuLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productIskontoGrupLabel: UILabel!
    @IBOutlet weak var incButton: UIButton!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var infoView: ProductInfoView!
    
    
    @IBOutlet weak var lblMensei: UILabel!
    
    @IBOutlet weak var lblAIA: UILabel!
    
    @IBOutlet weak var sepeteEkleButton: UIButton!
    @IBAction func OpenMenu(_ sender: Any) {
        self.presentLeftMenuViewController(self)

    }
    @IBAction func OpenSearch(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }

    }
    var product:Product!
    override func viewWillAppear(_ animated: Bool) {
    
        
        
        if(product.avaibleBayi){
            productBayiStokImage.image = UIImage(named: "var")
            productBayiStokLabel.text = "VAR"

        }else{
            productBayiStokImage.image = UIImage(named: "yok")
            productBayiStokLabel.text = "YOK"

        }
        if(product.avaibleTofas){
            productTofasStokImage.image = UIImage(named: "var")
            productTofasStokLabel.text = "VAR"

        }else{
            productTofasStokImage.image = UIImage(named: "yok")
            productTofasStokLabel.text = "YOK"

        }
        productNameLabel.text = product.Name
        productCodeLabel.text = product.Code
        //lblMensei.text = product
        
        productKdvliListeFiyati.text = product.listeFiyatiWithVat.priceFormatFixer()
        //productKdvliListeFiyati.text = String(format: "%.2f", product.listeFiyatiWithVat)+"TL"
        
       
        
        productKdvliFiyatLabel.text = product.kdvliFiyat.priceFormatFixer()
        //productKdvliFiyatLabel.text = String(format: "%.2f", product.kdvliFiyat)+"TL"
        
        productIskontoGrupLabel.text = product.iskontoGrubu
        
        
        productKdvsizFiyatLabel.text = product.kdvsizFiyat.priceFormatFixer()
        //productKdvsizFiyatLabel.text = String(format: "%.2f", product.kdvsizFiyat)+"TL"
        
        productListeFiyatiLabel.text = product.listeFiyati.priceFormatFixer()
        //productListeFiyatiLabel.text = String(format: "%.2f", product.listeFiyati)+"TL"

        //kdvliLabel.text =
        
        productCodeLabel.text = product.Code
        productCodeLabel.text = product.Code
        productCodeLabel.text = product.Code
       /* bayiStokLabel.backgroundColor = DesignData.red
        tofasStokLabel.backgroundColor = DesignData.red*/
        
        
        listeLabel.backgroundColor = DesignData.yellow
        iskontoLabel.backgroundColor = DesignData.yellow
        kdvliLabel.backgroundColor = DesignData.yellow
        kdvsizLabel.backgroundColor = DesignData.yellow
        kdvliListeFiyatLabel.backgroundColor = DesignData.yellow

        
        countLabel.backgroundColor = UIColor.clear
        incButton.backgroundColor = DesignData.darkBlue
        decreaseButton.backgroundColor = DesignData.darkBlue
        
      infoView.layer.borderWidth=1
        infoView.layer.borderColor = DesignData.darkGrey.cgColor
        
        countLabel.layer.borderWidth=1
        countLabel.layer.borderColor = DesignData.darkGrey.cgColor

        countLabel.text = String(product.minOrder)
        decreaseButton.layer.cornerRadius = 2
        incButton.layer.cornerRadius = 2
        

        sepeteEkleButton.backgroundColor = Color.red
        sepeteEkleButton.setTitleColor(UIColor.white, for: .normal)
     
    }

  
    @IBAction func sepeteEkle(_ sender: Any) {
        var repo=BasketRepository()
        repo.AddItem(quantitiy: Int(countLabel.text!)!, productCode: self.product!.Code,productId: self.product!.ProductId,completionHandler:{ response in
            
            self.showBasketMessage(done:response)

            
        })
    }

    
    func showBasketMessage(done:Bool){
        
       
        var alert : UIAlertController!
        if(done){
              alert = UIAlertController(title: "SEPET", message: "Ürün sepete başarıyla eklendi!", preferredStyle: UIAlertController.Style.alert)
        }
        else{
             let alert = UIAlertController(title: "SEPET", message: "Ürün sepete eklenmedi.", preferredStyle: UIAlertController.Style.alert)
        }
       
        alert.addAction(UIAlertAction(title: "DEVAM ET", style: .default, handler: { action in
            switch action.style{
            case .default:
                if let navController = self.navigationController {
                    navController.popViewController(animated: true)
                }

                
                return
            case .cancel:
                
                print("cancel")
                return
            case .destructive:
                print("destructive")
                
            }}))
        alert.addAction(UIAlertAction(title: "Sepete Git", style: .default, handler: { action in
            switch action.style{
            case .default:
              self.goToShoppingCart()
                return
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            }}))
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func decrease(_ sender: Any) {
        if(Int(countLabel.text!)!>0)
        {
            var numb=Int(countLabel.text!)!-1

            if(numb<product.minOrder){
                HelperMethods.showAlert(controller: self, message: "Bu üründen en az "+String(product.minOrder)+" adet alabilirsiniz!")
                return
            }
            countLabel.text = String(describing: numb)
        }
       
      
 
        
    }
    @IBAction func increase(_ sender: Any) {
        var numb=Int(countLabel.text!)!+1
        countLabel.text = String(describing: numb)
    }
    
    func goToShoppingCart(){
        performSegue(withIdentifier: "shoppingCartSegue", sender: self)
    }
    @IBAction func addToCart(_ sender: Any) {
        let alert = UIAlertController(title: "Ürün Sepeti", message: "Ürün sepetinize başarıyla eklendi.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Devam et", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Sepete Git", style: UIAlertAction.Style.destructive, handler: { action in

            
            
            // SEPETE GİT
            self.goToShoppingCart()

        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
