//
//  PointTableData.swift
//  Eryaz
//
//  Created by Arda Yucel on 6.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

class TesfikTableData{
    
    var Yil:String!
    var Gerceklesen:String!
    var Hedeflenen:String!
    var Kalan:String!
    
    var gerceklesenInt:Int!
    var hedeflenenInt:Int!
    var kalanInt:Int!
    
    init(yil:String,gerceklesen:Int,hedeflenen:Int,kalan:Int){
        Yil = yil
        kalanInt = kalan
        gerceklesenInt = gerceklesen
        hedeflenenInt = hedeflenen
        Gerceklesen = GetStringFromValue(value:gerceklesen)
        Hedeflenen = GetStringFromValue(value:hedeflenen)
        Kalan = GetStringFromValue(value:kalan)
    }
    func GetStringFromValue(value:Int) ->String{
        var result=""
        var strValue=String(value).reversed()
        var counter=0
        for char in strValue {
            result.insert(char, at: result.startIndex)
            counter = counter+1
            if(counter%3 == 0 && strValue.count != counter){
                result.insert(".", at: result.startIndex)
            }
        }
        
        return result 
    }
}
