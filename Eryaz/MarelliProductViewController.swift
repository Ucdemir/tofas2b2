//
//  MarelliProductViewController.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 19.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
class MarelliProductViewController:UIViewController{
    
    @IBAction func openMenu(_ sender: Any) {
        self.presentLeftMenuViewController(self)
    }
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var textView:UITextView!
    
    override func viewDidLoad() {
        let tapOnProduct = UITapGestureRecognizer(target: self, action: #selector(onClick(_:)))
        view.addGestureRecognizer(tapOnProduct)
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor =  UIColor.black.cgColor
    }
    @objc func onClick(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)

    }
    
}
