//
//  ComplateOrderController.swift
//  Eryaz
//
//  Created by Arda Yucel on 5.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
import DropDown
import MIBadgeButton_Swift

class ComplateOrderController:UIViewController {

    var appNotificationBarButton: MIBadgeButton!      // Make it global

    @IBOutlet weak var gonderimSekliButton: UIButton!
    @IBOutlet weak var gonderimAdresiButton: UIButton!
    @IBOutlet weak var gonderimNotuButton: UIButton!
    
    @IBOutlet weak var siparisTypeButton: UIButton!
    var gonderimSekliDropDown =  DropDown()
    var gonderimAdresiDropDown = DropDown()
    var gonderimNotuDropDown = DropDown()
    var siparisDropDown = DropDown()
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var textView: UITextView!

    
    var adresData : [ShippingInfo]?
    var sekilData : [ShippingInfo]?
    var notData : [ShippingInfo]?
    var orderType : [ShippingInfo]?
    
    var selectedAdres : ShippingInfo?
    var selectedSekil : ShippingInfo?
    var selectedNot : ShippingInfo?


    var adresDataString = [String]()
    
    var sekilDataString = [String]()
    var notDataString = [String]()
    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        textView.text = "Sipariş Notunuzu Giriniz"
        
        gonderimSekliButton.setTitleColor(hexStringToUIColor(hex: "2E151B"), for: .normal)
        gonderimSekliButton.layer.borderWidth=1
        gonderimSekliButton.layer.borderColor = DesignData.textColor.cgColor

        gonderimNotuButton.setTitleColor(hexStringToUIColor(hex: "2E151B"), for: .normal)
        gonderimNotuButton.layer.borderWidth=1
        gonderimNotuButton.layer.borderColor = DesignData.textColor.cgColor
        
        
        siparisTypeButton.setTitleColor(hexStringToUIColor(hex: "2E151B"), for: .normal)
        siparisTypeButton.layer.borderWidth=1
        siparisTypeButton.layer.borderColor = DesignData.textColor.cgColor
               
        
        
        gonderimAdresiButton.setTitleColor(hexStringToUIColor(hex: "2E151B"), for: .normal)
        gonderimAdresiButton.layer.borderWidth=1
        gonderimAdresiButton.layer.borderColor = DesignData.textColor.cgColor
        setupDropdown()

        applyButton.backgroundColor = hexStringToUIColor(hex: "e05038")
        applyButton.titleLabel?.textColor = UIColor.white
        
        SetBarButtonItems()
        SetNavTitle()
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func showBasketMessage(done:Bool){
        
        
        var alert : UIAlertController!
        if(done){
            alert = UIAlertController(title: "SİPARİŞ ONAY", message: "Siparişiniz başarıyla oluşturulu!", preferredStyle: UIAlertController.Style.alert)
        }
        else{
            let alert = UIAlertController(title: "SİPARİŞ ONAY", message: "Sipariş oluşturulamadı.", preferredStyle: UIAlertController.Style.alert)
        }
        
        alert.addAction(UIAlertAction(title: "DEVAM ET", style: .default, handler: { action in
            switch action.style{
            case .default:
               self.goToMain()
                
                return
            case .cancel:
                
                print("cancel")
                return
            case .destructive:
                print("destructive")
                
            }}))
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func goToMain(){
        if(SessionInformation.TofasCompany == "OPAR"){
            self.performSegue(withIdentifier: "oparComplate", sender: self)
        }
        else
        {
            self.performSegue(withIdentifier: "marelliComplate", sender: self)
        }
    }
    
    @IBAction func actionOrderType(_ sender: Any) {
         siparisDropDown.show()
        
    }
    
    
    @IBAction func onApplyButton(_ sender: Any) {
        
        print("Adres",self.selectedAdres?.text," sekil ",self.selectedSekil?.text," not ",self.selectedNot?.text)
        
        var repo = BasketRepository()
        repo.ApproveOrder(shipmentValue: (self.selectedSekil?.id)!, shipmentAddressValue: (self.selectedAdres?.id)!, note:self.textView.text ,completionHandler:{ response in
           
                self.showBasketMessage(done: response)
            
            })
    }

    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    func setupDropdown(){
        
        var repo=BasketRepository()
        
        repo.GetShippingType(completionHandler:{ response in
            DispatchQueue.main.async {
                self.sekilData = response
                self.sekilDataString = [String]()
                for data in response{
                    self.sekilDataString.append(data.text!)
                }
                self.gonderimSekliDropDown.dataSource = self.sekilDataString
                self.gonderimSekliDropDown.reloadAllComponents()
            }
        })
        
        
        repo.GetShippingNote(completionHandler:{ response in
            DispatchQueue.main.async {
                self.notData = response
                self.notDataString = [String]()
                for index in 0...response.count-1{
                    var data = response[index]
                    if(data.text == nil){
                        self.notData!.remove(at: index)
                    }else{
                        self.notDataString.append(data.text!)
                    }
                }
                self.gonderimNotuDropDown.dataSource = self.notDataString
                self.gonderimNotuDropDown.reloadAllComponents()

            }
            DispatchQueue.main.async {
                
                let data = ShippingInfo ()
                data.id = 0
                data.text = "NORMAL"
                
                let data2 =  ShippingInfo ()
                data.id = 1
                data.text = "ACİL"
                        self.orderType = [data, data2]
                    
                     
                        self.siparisDropDown.dataSource = ["NORMAL","ACİL"]
                        self.siparisDropDown.reloadAllComponents()

                    }
            
            
        })
        
  
        
        repo.GetShippingAdress(completionHandler:{ response in
            DispatchQueue.main.async {
                self.adresData = response
                self.adresDataString = [String]()
                for index in 0...response.count-1{
                    var data = response[index]
                    if(data.text == nil){
                        self.adresData!.remove(at: index)
                    }else{
                        self.adresDataString.append(data.text!)
                    }
                }
                self.gonderimAdresiDropDown.dataSource = self.adresDataString
                self.gonderimAdresiDropDown.reloadAllComponents()


        }
        })
        
        gonderimAdresiDropDown.selectionAction = { [weak self] (index, item) in
            self?.gonderimAdresiButton.setTitle(item, for: .normal)
            self?.selectedAdres = self!.getItem(arr: (self?.adresData)!, str: item)
        }
        gonderimSekliDropDown.selectionAction = { [weak self] (index, item) in
            self?.gonderimSekliButton.setTitle(item, for: .normal)
            self?.selectedSekil = self!.getItem(arr: (self?.sekilData)!, str: item)
        }
        gonderimNotuDropDown.selectionAction = { [weak self] (index, item) in
            self?.gonderimNotuButton.setTitle(item, for: .normal)
            self?.selectedNot = self!.getItem(arr: (self?.notData)! , str: item)
            self?.textView.text  = self?.selectedNot?.text
        }
        siparisDropDown.selectionAction = { [weak self] (index, item) in
            self?.siparisTypeButton.setTitle(item, for: .normal)
            self?.selectedNot = self!.getItem(arr: (self?.orderType)! , str: item)
            self?.textView.text  = self?.selectedNot?.text
        }
      
    }
    @objc func onMenuButton(){
        self.presentLeftMenuViewController(self)
        
    }
    @IBAction func openSearch(_ sender: Any) {
        if(SessionInformation.TofasCompany=="OPAR"){
            performSegue(withIdentifier: "complateOparSegue", sender: self)
        }
        else{
            performSegue(withIdentifier: "complateMarelliSegue", sender: self)
            
        }
        
    }
    @IBAction func onShopButton(_ sender: Any) {
        performSegue(withIdentifier: "complateSepetSegue", sender: self)
    }
    func SetBarButtonItems(){
        let btn3 = UIButton(type: .custom)
        btn3.setImage(UIImage(named: "menuButton"), for: .normal)
        btn3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn3.addTarget(self, action: #selector(onMenuButton), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn3)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
        
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "basketicon"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        
        self.appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        self.appNotificationBarButton.setImage(UIImage(named: "basketicon"), for: .normal)
        self.appNotificationBarButton.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        self.appNotificationBarButton.addTarget(self, action: #selector(onShopButton(_:)), for: .touchUpInside)
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: self.appNotificationBarButton)
        
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2,App_NotificationBarButton], animated: true)
        
    }
    func SetNavTitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "SİPARİŞ ONAY"
        }
        else{
            label.text = "SİPARİŞ ONAY\n(\(SessionInformation.Bayi_?.name ?? ""))"
        }
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
        
    }
    @IBAction func onSekilDropDown(_ sender: Any) {
        if(self.sekilData != nil && (self.sekilData?.count)!>0){
            gonderimSekliDropDown.show()

        }
    }
    
    @IBAction func onAdresDropDown(_ sender: Any) {
        if(self.adresData != nil && (self.adresData?.count)!>0){
            gonderimSekliDropDown.show()
            
        }
        gonderimAdresiDropDown.show()
        
    }
    @IBAction func onNotDropDown(_ sender: Any) {
        if(self.sekilData != nil && (self.sekilData?.count)!>0){
            gonderimNotuDropDown.show()
            
        }
    }
    func getItem(arr:[ShippingInfo],str:String) -> ShippingInfo?{
        for item in arr{
            if(item.text == str){
                return item
            }
        }
        return nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Number of columns of data

    
}
