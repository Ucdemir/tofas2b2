//
//  ContactViewController.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 16.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class ContactViewController:UIViewController{
    
    @IBOutlet weak var telefonLabel: UILabel!
    @IBOutlet weak var telefonText: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adresLabel: UILabel!
    @IBOutlet weak var adresText: UILabel!
    @IBOutlet weak var bayiSecButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var websiteText: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var faksText: UILabel!
    @IBOutlet weak var faksLabel: UILabel!
    var appNotificationBarButton: MIBadgeButton!      // Make it global

    override func viewDidLoad() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"menuButton"), style: .plain, target: self, action: #selector(onMenuButton))
        
        telefonLabel.textColor = UIColor.white
        nameLabel.textColor = DesignData.darkBlue
        adresLabel.textColor = UIColor.white
        websiteLabel.textColor = UIColor.white
        faksLabel.textColor = UIColor.white
        telefonLabel.backgroundColor = DesignData.darkBlue
        nameLabel.backgroundColor = UIColor.white
        adresLabel.backgroundColor = DesignData.darkBlue
        websiteLabel.backgroundColor = DesignData.darkBlue
        faksLabel.backgroundColor = DesignData.darkBlue
        
        bayiSecButton.backgroundColor = .clear
        bayiSecButton.layer.cornerRadius = 5
        bayiSecButton.layer.borderWidth = 1
        bayiSecButton.layer.borderColor = DesignData.darkBlue.cgColor
        
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.clear
        
     SetBarButtonItems()


    }
    override func viewWillAppear(_ animated: Bool) {
        if(SessionInformation.selectedContactBayi != nil){
            print("selected bayi: ",SessionInformation.selectedContactBayi!)

        }
        SetNavTitle()
        if(appNotificationBarButton != nil){
            var basketRepo = BasketRepository()
            basketRepo.GetItemCount(completionHandler:{ response in
                self.appNotificationBarButton.badgeString = String(response)
            })
            
        }
    }
    func reloadTable(){
        self.tableView.reloadData()
    }
    func showPopup() {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contactBayiSelectionViewController") as! BayiSelectionViewController
        popOverVC.isContact = true
        popOverVC.contactController = self
        // popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width
            , height:   view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        
    }
    @objc func onMenuButton(){
        self.presentLeftMenuViewController(self)

    }
    @IBAction func openSearch(_ sender: Any) {
        if(SessionInformation.TofasCompany=="OPAR"){
            performSegue(withIdentifier: "contactSearch", sender: self)
        }
        else{
            performSegue(withIdentifier: "contactSearchMarelli", sender: self)

        }
        
    }
    @IBAction func onShopButton(_ sender: Any) {
        performSegue(withIdentifier: "contactSepetim", sender: self)
    }
    func SetBarButtonItems(){
        let btn3 = UIButton(type: .custom)
        //btn3.setImage(UIImage(named: "menuButton"), for: .normal)
        btn3.setTintWithImage(imageName: "menuButton", tintColor: UIColor.white)

        btn3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn3.addTarget(self, action: #selector(onMenuButton), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn3)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)

        
        let btn1 = UIButton(type: .custom)
        //btn1.setImage(UIImage(named: "basketicon"), for: .normal)
        btn1.setTintWithImage(imageName: "basketicon", tintColor: UIColor.white)

        
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        
        self.appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
       
        self.appNotificationBarButton.setColorWithImage(imageName: "basketicon", tintColor: UIColor.white)

        
        self.appNotificationBarButton.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        self.appNotificationBarButton.addTarget(self, action: #selector(onShopButton(_:)), for: .touchUpInside)
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: self.appNotificationBarButton)
        
        
        let btn2 = UIButton(type: .custom)
      
        btn2.setTintWithImage(imageName: "search", tintColor: UIColor.white)

        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(openSearch(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2,App_NotificationBarButton], animated: true)

    }
    func SetNavTitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "İLETİŞİM"
        }
        else{
            label.text = "İLETİŞİM\n(\(SessionInformation.Bayi_?.name ?? ""))"
        }
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
        
    }
    @IBAction func onBayiSec(_ sender: Any) {
        showPopup()
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
extension ContactViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell:ContactTableCell = self.tableView.dequeueReusableCell(withIdentifier: "contactCell") as! ContactTableCell
        
        var selectedBayi = SessionInformation.selectedContactBayi
        if(selectedBayi == nil){
            cell.label.text = ""
            return cell
        }
        if(indexPath.row == 0){
            cell.label.text = selectedBayi!.name
        }
        if(indexPath.row == 1){
            cell.label.text = "\(selectedBayi!.cmpAdress1) \(selectedBayi!.cmpAdress2!)"
        }
        if(indexPath.row == 2){
            cell.label.text = selectedBayi!.tel1
        }
      
        
        
        cell.clipsToBounds = true
        
        cell.layer.borderWidth = 1
        
        
        cell.layer.borderColor = DesignData.darkBlue.cgColor
        cell.backgroundColor = UIColor.white
        return cell
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  3
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 70.0;//Choose your custom row height
    }
    
    
}

