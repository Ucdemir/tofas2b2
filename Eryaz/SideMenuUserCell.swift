//
//  SideMenuLogoCell.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class SideMenuUserCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var menuLogoImageView: UIImageView!
    @IBOutlet weak var customImageView: UIImageView!
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                customImageView.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                customImageView.addConstraint(aspectConstraint!)
            }
        }
    }
    @IBAction func changeButton(_ sender: Any) {
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    func setCustomImage(image : UIImage) {
        
        let aspect = image.size.width / image.size.height
        
        let constraint = NSLayoutConstraint(item: customImageView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customImageView, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        
        aspectConstraint = constraint
        
        customImageView.image = image
        customImageView.layer.borderWidth = 1
        customImageView.layer.masksToBounds = false
        customImageView.layer.borderColor = UIColor.black.cgColor
        customImageView.layer.cornerRadius = customImageView.frame.height/2
        customImageView.clipsToBounds = true
    }
}
