//
//  MarelliProductViewController.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 19.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
class OparAnnouncementViewController:UIViewController{
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var textView:UILabel!
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var button:UIButton!

    override func viewDidLoad() {
        let tapOnProduct = UITapGestureRecognizer(target: self, action: #selector(onClick(_:)))
        view.addGestureRecognizer(tapOnProduct)
        self.bgView.layer.borderWidth = 1
        self.bgView.layer.borderColor =  UIColor.black.cgColor
        button.layer.cornerRadius = 15
        self.button.layer.borderWidth = 1
        self.button.layer.borderColor =  UIColor.black.cgColor
    }
    @objc func onClick(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
