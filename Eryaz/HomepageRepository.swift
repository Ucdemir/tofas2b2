//
//  LoginRepository.cs.swift
//  Eryaz
//
//  Created by Arda Yucel on 6.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//
import  Alamofire
import Foundation

public class HomepageRepository{
    
    func Relogin(completionHandler: @escaping (Bool) -> Void) {
        var repo=LoginRepository()
        repo.Login(username:SessionInformation.UserName,password: SessionInformation.Password,completionHandler:{ response in
            
            let data = response.object(forKey: "Data") as? NSDictionary
            if (data != nil) {
                print("token")
                SessionInformation.Token = data?.object(forKey: "Token") as! String
                completionHandler(true)
            }
            else
            {
               
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                }
                completionHandler(false)

            }
            
        })
    }
    func GetPointList(completionHandler: @escaping ([PointTableData]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,
                                         "Data": ["Name":"BosPuanSummary_GetList_HomePage"]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/BosPuanSummary_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [PointTableData]()
                        
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                     return
                                    }
                                    else{
                                        self.GetPointList(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                                for item in data! {
                                var pointData=PointTableData(yil: item["YIL"] as! Int, kazanilan: item["KAZANILAN"] as! Int, harcanan: item["HARCANAN"] as! Int, kalan: item["KALAN"] as! Int)
                                      result.append(pointData
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [PointTableData]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    

    func GetTesfikList(completionHandler: @escaping ([TesfikTableData]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,
                                         "Data": ["Name":"TargetOparTesvik_GetList_HomePage"]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/TargetOparTesvik_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [TesfikTableData]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetTesfikList(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }

                        if(data != nil) {
                            
                            for item in data! {
                                var pointData=TesfikTableData(yil: item["Name"] as! String, gerceklesen: item["OccuredPercent"] as! Int, hedeflenen: item["Target"] as! Int, kalan: item["LeftTotal"] as! Int)
                                result.append(pointData
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [TesfikTableData]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func GetPopularList(completionHandler: @escaping ([Announcement]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": SessionInformation.Bayi_?.code,"TofasCompany": SessionInformation.TofasCompany,
                                         "Data": ["Name":"ProductSelected_GetList_HomePage"]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/ProductSelected_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Announcement]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetPopularList(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        
                        if(data != nil) {
                            
                            
                            for item in data! {
                               // let data2 = item["Campaign"] as! [String:Any]
                     
                                if let header = (item["PicturePath"]) as? String { // If casting, use, eg, if let var = abc as? NSString
                                    let path = "https://b2b.opar.com/" + (item["PicturePath"] as! String)
                                    //let id = item["Code"] as! Int
                                    let header = item["NameShort"] as! String
                                    let code = item["Code"] as! String
                                    let price = item["PriceListWithVatValue"] as! String

                                    var announcement=Announcement()
                                    //announcement.Id=id
                                    announcement.Header = header
                                    announcement.Path=path
                                    announcement.Code = code
                                    announcement.Price = price
                                    result.append(announcement
                                    )                                }
                                else {
                                        continue
                                    
                                }
                                
                           
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [Announcement]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
     func GetAnnouncementList(completionHandler: @escaping ([Announcement]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,
                                         "Data": ["Name":"Announcement_GetList_HomePage",
                                                  "Parameters": ["DealerId":SessionInformation.DealerId]]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Announcement_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                      
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Announcement]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetAnnouncementList(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }

                        if(data != nil) {
                            

                            for item in data! {
                                
                                let path = "https://b2b.opar.com/" + (item["PicturePathMobile"] as! String)
                                let id = item["Id"] as! Int
                                let header = item["Header"] as! String

                                var announcement=Announcement()
                                announcement.Id=id
                                announcement.Header = header
                                announcement.Path=path
                                result.append(announcement
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [Announcement]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetMarelliProducts(completionHandler: @escaping ([Announcement]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": "",
                                         "Data": ["Name":"Product_GetList_HomePage"]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Product_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Announcement]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetMarelliProducts(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                            
                            for item in data! {
                                
                                let path = "https://b2b.opar.com/" + (item["PicturePath"] as! String)
                                let id = item["Id"] as! Int
                                let descp = item["Description"] as! String
                                var announcement=Announcement()
                                announcement.Id=id
                                announcement.Path=path
                                announcement.Description=descp
                                result.append(announcement
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [Announcement]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func GetOldCampaign(completionHandler: @escaping ([Announcement]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": "",
                                         "Data": ["Name":"Campaign_GetList_HomePage"]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Campaign_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Announcement]()
                        
                        if(data != nil) {
                            
                            
                            for item in data! {
                                
                                let path = "https://b2b.opar.com/" + (item["PicturePath"] as! String)
                                let id = item["Id"] as! Int
                                var announcement=Announcement()
                                announcement.Id=id
                                announcement.Path=path
                                result.append(announcement
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [Announcement]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }

    func getCampaign(completionHandler: @escaping (ModelCampaignSuper) -> Void)  {
              
             
              let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
                     
             let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,"Data": ["Name":"CampaignBosHeader_GetList_HomePage"]]
              
              Alamofire.request("https://b2b.opar.com/Api/Mobile/CampaignBosHeader_GetList_HomePage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
                  .response(
                      queue: queue,
                      responseSerializer: DataRequest.jsonResponseSerializer(),
                      completionHandler: { response in
                          // You are now running on the concurrent `queue` you created earlier.
                          switch response.result {
                              
                              
                          case .success(let JSON):
                             // print(response)
                              print(response.data.map { String(decoding: $0, as: UTF8.self) } ?? "No data.") //this method print decodable
                             
                            
                            guard let data = response.data else { return }
                                     do {
                                         let decoder = JSONDecoder()
                                         let data = try decoder.decode(ModelCampaignSuper.self, from: data)
                                        DispatchQueue.main.async {
                                            completionHandler( data)
                                        }
                                     } catch let error {
                                         print(error)
                                        
                                     }
                                  
                              
                              
                              
                              
                              
                          case .failure(let error):
                              print("Request failed with error: \(error)")
                              
                              
                          }
                          // To update anything on the main thread, just jump back on like so.
                          
                  }
              )
          }
    }



