//
//  SelectOparViewController.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 13.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

import UIKit
import RevealingSplashView

class SelectOparViewController:UIViewController{
    
    @IBOutlet weak var magnetiButton: UIButton!
    @IBOutlet weak var oparButton: UIButton!
    var revealingSplashView : RevealingSplashView!
    
    override func viewDidLoad() {

        oparButton.imageView?.setImageColor(color: UIColor.white)
        oparButton.backgroundColor = DesignData.darkBlue

    }
    @IBAction func onOparButton(_ sender: Any) {
        SessionInformation.TofasCompany = "OPAR"
        var image :UIImage!
        if(SessionInformation.TofasCompany == "OPAR"){
            image=UIImage(named: "opar_logo_white")
        }
        else{
            image=UIImage(named: "magneti_white")
            
        }
        revealingSplashView = RevealingSplashView(iconImage: image,iconInitialSize: CGSize(width: 150, height: 150), backgroundColor: DesignData.darkBlue)
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        self.view.addSubview(revealingSplashView)

        revealingSplashView.startAnimation(){
            self.performSegue(withIdentifier: "selectionToLoginSegue", sender: self)
        }
    }
    @IBAction func onMagnetiButton(_ sender: Any) {
        SessionInformation.TofasCompany = "MARELLI"
        var image :UIImage!
        if(SessionInformation.TofasCompany == "OPAR"){
            image=UIImage(named: "opar_logo_white")
        }
        else{
            image=UIImage(named: "magneti_white")
            
        }
        revealingSplashView = RevealingSplashView(iconImage: image,iconInitialSize: CGSize(width: 150, height: 150), backgroundColor: DesignData.darkBlue)
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        self.view.addSubview(revealingSplashView)

        revealingSplashView.startAnimation(){
            self.performSegue(withIdentifier: "selectionToLoginSegue", sender: self)
        }
    }
}

