//
//  LoginViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 25/06/2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import LocalAuthentication

import RevealingSplashView
class LoginViewController:UIViewController, UITextFieldDelegate{
   
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rememberSwitch: UISwitch!
    var repo:LoginRepository!
    var username=""
    var password=""
    var remembered:Bool?
    var touch_id_option=0
    var willAskTouchId=true
    var loginInProgress=false
    var manager : DBManager!

    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)

        usernameTF.delegate = self
        passwordTF.delegate = self
        //Initialize a revealing Splash with with the iconImage, the initial size and the background color
        var image :UIImage!
        
        if(SessionInformation.TofasCompany == "OPAR"){
            image=UIImage(named: "opar_logo_white")
        }
        else{
            image=UIImage(named: "magneti_white")
            
        }
        let revealingSplashView = RevealingSplashView(iconImage: image,iconInitialSize: CGSize(width: 150, height: 150), backgroundColor: DesignData.darkBlue)
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        
        //Adds the revealing splash view as a sub view
        self.view.addSubview(revealingSplashView)
        repo = LoginRepository()
        setTF()

        usernameTF.text? = "P35401688656"
        passwordTF.text? = "Fiat2020"
        username = "P35401688656"
        password = "Fiat2020"
        
        //Starts animation
        revealingSplashView.startAnimation(){
            print("Completed")
        }
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        UserDefaults.standard.setValue(false, forKey:"_UIConstraintBasedLayoutLogUnsatisfiable")

        var user=DBManager.fetchData()
                if(user.count>0)//user.count>0)
        {
            let us=user.popLast()! as USER

       

            self.remembered=us.remembered
            self.touch_id_option=Int(us.touch_id_option)

            if(remembered)!
            {
                
                print("remembered")
                usernameTF.text=us.username
                passwordTF.text=us.password
                self.username=us.username!
                self.password=us.password!
                self.rememberSwitch.isOn=true
                OnLogin(self)
                return
                
            }else if(us.touch_id_option == 1){
                self.rememberSwitch.isOn = false
                usernameTF.text=us.username
                username=us.username!
                password=us.password!
                LoginWithTouchId()
                return
            }
            else{
                self.username = ""
                self.password = ""
                self.rememberSwitch.isOn = false
            }
            
    
            
            
            

        }
        else{

            self.rememberSwitch.isOn=false
        }
        
        
    }
    
    func LoginWithTouchId(){
        checkFingerPrint(touch_option: touch_id_option){(isValid) in
            
            print("checking findger print")
            if(isValid==true)
            {
                self.OnLogin(self)
            }
            else{
                print("not valid")
            }
        }
        
    }
    @IBAction func OnLogin(_ sender: Any) {
        
        print("logining with :",self.username,"  ",self.password)
        if(loginInProgress){
       			
            return

        }
        else  if(( self.remembered==false && self.touch_id_option != 1 )&&(self.usernameTF.text!=="" || self.passwordTF.text!=="")){
            HelperMethods.showAlert(controller: self, message: "Kullanıcı adı ve şifre boş bırakılamaz!")
            
        }
        else{
            
            var name = self.username == "" ? usernameTF.text :self.username
            var pw = self.password == "" ? passwordTF.text :self.password
            repo.Login(username:name!,password: pw! ,completionHandler:{ response in
                
                let data = response.object(forKey: "Data") as? NSDictionary
                if (data != nil) {
                    print("token")
                    self.username = name!
                    self.password = pw!
                    SessionInformation.UserName = name!
                    SessionInformation.Password = pw!
                    SessionInformation.Token = data?.object(forKey: "Token") as! String
                    SessionInformation.username = data?.object(forKey: "UserName") as! String
                    if(self.touch_id_option==0 && self.rememberSwitch.isOn == false){
                        self.askTouchIdLogin()
                    }
                    else{
                        self.PerformLogin()

                    }
                }
                else
                {
                    print(self.username," ",self.password)
                    HelperMethods.showAlert(controller: self, message: "Yanlış kullanıcı adı yada şifre!")
                    self.loginInProgress=false;
                }
                
            })
        }
       
    }
    
    func PerformLogin(){
        self.saveLoginState()
        performSegue(withIdentifier: "loginSegue", sender: self)
    }
   
   
    func checkFingerPrint(touch_option:Int,completion: @escaping (Bool) -> Void) {
        
        let context = LAContext()
        var error: NSError?
        if(touch_option==2||touch_option==0){
            completion(false)
            
        }
        var policy: LAPolicy?
        // Depending the iOS version we'll need to choose the policy we are able to use
        if #available(iOS 9.0, *) {
            // iOS 9+ users with Biometric and Passcode verification
            policy = .deviceOwnerAuthentication
        } else {
            // iOS 8+ users with Biometric and Custom (Fallback button) verification
            context.localizedFallbackTitle = "Fuu!"
            policy = .deviceOwnerAuthenticationWithBiometrics
        }
        
        var err: NSError?
        
        // Check if the user is able to use the policy we've selected previously
        guard context.canEvaluatePolicy(policy!, error: &err) else {
            // Print the localized message received by the system
            print("user cant use policy ",err)
            completion(false)
            return
        }
        context.evaluatePolicy(policy!, localizedReason: "Parmak iziniz ile giriş yapınız.", reply: { (success, error) in
            if(success){
                completion(true)

            }
            else{
                completion(false)

            }

        })
     

        return
        
     
    }
    
    
    
    
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    
    func askTouchIdLogin(){
        
        if(touch_id_option==0)
        {
            let alert = UIAlertController(title: "GÜVENLİK ADIMI", message: "Sonraki girişlerinizde parmak izi kullanarak giriş yapmak istermisiniz?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "EVET", style: .default, handler: { action in
                switch action.style{
                case .default:
                    self.touch_id_option=1
                    self.PerformLogin()

                    return
                case .cancel:
                    self.PerformLogin()

                    print("cancel")
                    return
                case .destructive:
                    print("destructive")
                    
                }}))
            alert.addAction(UIAlertAction(title: "HAYIR", style: .default, handler: { action in
                switch action.style{
                case .default:
                    self.touch_id_option=2
                    self.PerformLogin()
                    return
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                }}))
            present(alert, animated: true, completion: nil)
            
        }else{
            self.PerformLogin()
            return
        }
        
    }
    @IBAction func forgetPassword(_ sender: Any) {
    }
    
    func saveLoginState(){
        
        if(self.rememberSwitch.isOn){
                DBManager.setLastLoginToDB(bayiKodu:"",username: self.username, password: self.password,type:false, finger_print_option: 2,remembered:true)
        }
        else if(touch_id_option == 1  ){
            DBManager.setLastLoginToDB(bayiKodu:"",username: self.username, password: self.password,type:false, finger_print_option: 1,remembered: false  )
            
        }
        
        }
    
    
    
    func setTF(){
       
        usernameTF.rightViewMode = UITextField.ViewMode.always
        var usernameImageView = UIImageView()
        usernameImageView.image = #imageLiteral(resourceName: "username")
        usernameImageView.contentMode = UIView.ContentMode.scaleAspectFit
        usernameImageView.translatesAutoresizingMaskIntoConstraints = false
        usernameImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        usernameImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true

        usernameTF.rightView = usernameImageView
        
        var passwordImageView = UIImageView()
        passwordTF.rightViewMode = UITextField.ViewMode.always
        passwordImageView.image = #imageLiteral(resourceName: "password")
        passwordImageView.contentMode = UIView.ContentMode.scaleAspectFit
        passwordImageView.translatesAutoresizingMaskIntoConstraints = false
        passwordImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        passwordImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordTF.rightView = passwordImageView
        
        usernameTF.backgroundColor = DesignData.weakBlue
        passwordTF.backgroundColor = DesignData.weakBlue
        loginButton.backgroundColor = DesignData.darkBlue
        rememberSwitch.tintColor = DesignData.darkBlue
        rememberSwitch.onTintColor = DesignData.midBlue
        
    }

}

