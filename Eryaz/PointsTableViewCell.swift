//
//  PointsTableViewCell.swift
//  Eryaz
//
//  Created by Ferit Özcan on 25/06/2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class PointsTableViewCell : UITableViewCell{
    
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var left: UILabel!
    @IBOutlet weak var usage: UILabel!
    
    @IBOutlet weak var win: UILabel!
    func setFont(font:UIFont,displayP3Red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
    {
        let color=UIColor(displayP3Red: CGFloat(displayP3Red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: CGFloat(alpha))
        points.font=font
        win.font=font
        left.font=font
        usage.font=font
        
       // points.font = UIFont.boldSystemFont(ofSize: points.font.pointSize)
        points.textColor=color
        left.textColor=color
        usage.textColor=color
        win.textColor=color
    }
    
    func setAllignment(allign:NSTextAlignment)
    {
        points.textAlignment = allign
        win.textAlignment = allign
        left.textAlignment = allign
        usage.textAlignment = allign
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
