//
//  SearchRestrictionViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 28.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import DropDown
import UICheckbox_Swift
import MIBadgeButton_Swift
import BarcodeScanner

class SearchRestrictionViewController:UIViewController,BarcodeScannerCodeDelegate,BarcodeScannerErrorDelegate,BarcodeScannerDismissalDelegate,UITextFieldDelegate,UIScrollViewDelegate{

    @IBOutlet weak var upButton: UIButton!
    
    @IBOutlet weak var scroolView: UIScrollView!
    @IBOutlet weak var dataViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var searchImg3: UIImageView!
    @IBOutlet weak var searchImg2: UIImageView!
    @IBOutlet weak var searchImg1: UIImageView!
    @IBOutlet weak var cleanButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var catagory4: UILabel!
    @IBOutlet weak var catagory3: UILabel!
    @IBOutlet weak var catagory5: UILabel!
    @IBOutlet weak var catagory2: UILabel!
    @IBOutlet weak var catagory1: UILabel!
    @IBOutlet weak var markaLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var scroolViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var markaDropDownButton: UIButton!
    @IBOutlet weak var modelDropDownButton: UIButton!
    @IBOutlet weak var quickSearchText: UITextField!
    @IBOutlet weak var productNameText: UITextField!
    @IBOutlet weak var productCodeText: UITextField!
    @IBOutlet weak var firsatKosesiRadioButton: UICheckbox!
    @IBOutlet weak var bayiKampanyaRadioButton: UICheckbox!
    @IBOutlet weak var tofasKampanyaRadioButton: UICheckbox!
    
    @IBOutlet weak var takiptekiUrunlerRadioButton: UICheckbox!
    @IBOutlet weak var newProductRadioButton: UICheckbox!
    var firsatKosesi = false
    let pointsTableHeaderFont=UIFont(name: "HelveticaNeue-Medium", size:12)

    @IBOutlet weak var cleanImage: UIImageView!
    @IBOutlet weak var searchImage: UIImageView!
    var appNotificationBarButton: MIBadgeButton!      // Make it global
    var appNotificationBarButton2: MIBadgeButton!      // Make it global

    let markaDropDown = DropDown()
    let modelDropDown = DropDown()
    let repo=SearchRepository()

    var selectedProduct:Product?
    var selectedModel =  ""
    
    var products = [Product]()

    var markaList :[String]!
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBAction func upAction(_ sender: Any) {
        if(self.products.count>6){
            scroolView.setContentOffset(.init(x: 0, y: 300), animated: true)

        }
        else{
            scroolView.setContentOffset(.zero, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // This will be called every time the user scrolls the scroll view with their finger
        // so each time this is called, contentOffset should be different.
        
        var  currentY = self.scroolView.contentOffset.y
        if(upButton.isHidden){
            if(currentY>550){
                upButton.isHidden=false
                upButton.shake()
            }
        }
        else{
            if(currentY<=550){
                upButton.isHidden=true

            }
        }
        print(self.scroolView.contentOffset.y)
        
        //Additional workaround here.
    }
    func SetNavTitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        var bayi :String!
        if(SessionInformation.Bayi_?.code == nil){
            label.text = "ARAMA"
        }
        else{
            label.text = "ARAMA\n(\(SessionInformation.Bayi_?.name ?? ""))"
        }
        label.sizeToFit()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 14)
        self.navigationItem.titleView = label
        self.navigationItem.titleView?.setNeedsLayout()
        self.navigationItem.titleView?.setNeedsDisplay()
        
    }
    @objc func dismissKeyboard() -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        dataView.addGestureRecognizer(tap)
        cleanImage.setImageColor(color: UIColor.white)
        searchImage.setImageColor(color: UIColor.white)

        quickSearchText.delegate = self
        productCodeText.delegate = self
        productNameText.delegate = self

        scroolView.delegate = self
        upButton.backgroundColor = UIColor.darkGray
        upButton.isHidden = true
        upButton.layer.borderWidth=1
        upButton.layer.borderColor = UIColor.black.cgColor
        upButton.layer.cornerRadius = 4
        scroolView!.touchesShouldCancel(in: tableView!)
        setupDropdown()
        cleanButton.backgroundColor = hexStringToUIColor(hex: "e05038")
        applyButton.backgroundColor = hexStringToUIColor(hex: "22264b")
        
        markaLabel.textColor = Color.textColor
        modelLabel.textColor = Color.textColor
        //markaLabel.textColor = hexStringToUIColor(hex: "223A5E")
        // modelLabel.textColor = hexStringToUIColor(hex: "223A5E")
        catagory1.textColor = Color.textColor
        catagory2.textColor = Color.textColor
        catagory3.textColor = Color.textColor
        catagory4.textColor = Color.textColor
        catagory5.textColor = Color.textColor
        markaDropDownButton.setTitleColor(hexStringToUIColor(hex: "2E151B"), for: .normal)
        modelDropDownButton.setTitleColor(hexStringToUIColor(hex: "2E151B"), for: .normal)
        markaDropDownButton.setTitle("Seçiniz", for: .normal)
        modelDropDownButton.setTitle("Seçiniz", for: .normal)
        
        markaDropDownButton.layer.borderWidth=1
        modelDropDownButton.layer.borderWidth=1
        modelDropDownButton.layer.borderColor = DesignData.textColor.cgColor
        markaDropDownButton.layer.borderColor = DesignData.textColor.cgColor
        catagory1.layer.cornerRadius = 5
        catagory1.frame = catagory1.frame.inset(by: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
      //  catagory1.backgroundColor = Color.tableCellBackroundColor
        catagory2.layer.cornerRadius = 5
        catagory2.frame = catagory2.frame.inset(by: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
      //  catagory2.backgroundColor = Color.tableCellBackroundColor
        catagory3.layer.cornerRadius = 5
        catagory3.frame = catagory3.frame.inset(by: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
      //  catagory3.backgroundColor = Color.tableCellBackroundColor
        catagory4.layer.cornerRadius = 5
        catagory4.frame = catagory4.frame.inset(by: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
       // catagory4.backgroundColor = Color.tableCellBackroundColor
        
        catagory5.frame = catagory4.frame.inset(by: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
       // catagory5.backgroundColor = Color.tableCellBackroundColor
        catagory5.layer.cornerRadius = 5
        
        
        var bg = UIColor.white
        dataView.backgroundColor = bg
        tableView.backgroundColor = UIColor.lightGray
        searchImg1.backgroundColor = bg
        searchImg2.backgroundColor = bg
        searchImg3.backgroundColor = bg
        TopView.backgroundColor = bg
        view.backgroundColor = bg
        
        tableView.tableFooterView  = UIView()
        self.tableView.delegate = self
        self.TopView.clipsToBounds = false
        
        if(firsatKosesi){
            firsatKosesiRadioButton.isSelected = true
        }
        
        
        

 
        
        self.appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        self.appNotificationBarButton.setColorWithImage(imageName: "basketicon", tintColor: UIColor.white)

 
    
        
       
        
        self.appNotificationBarButton.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        self.appNotificationBarButton.addTarget(self, action: #selector(onBasketButton(_:)), for: .touchUpInside)
        
        let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: self.appNotificationBarButton)
        
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        
        let btn2 = UIButton(type: .custom)
        
        btn2.setTintWithImage(imageName:"barcode" , tintColor: UIColor.white)
        
        
        
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(openBarcode(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        btn2.translatesAutoresizingMaskIntoConstraints = true;

        self.navigationItem.setRightBarButtonItems([item2, App_NotificationBarButton], animated: true)

        
        quickSearchText.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        productNameText.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)
        productCodeText.placeholderColor = UIColor(rgb: 0x585c5e, alphaVal: 1.0)

     
    }

    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print(code)
     
        var request = SearchRequest()
        request.campaignBayiSelected = false
        request.campaignTofasSelected = false
        request.followProductSelected = false
        request.newProductSelected = false
        request.opportunitySelected = false
        request.productNameText = ""
        request.productCodeText = code
        self.repo.GetProductList(request: request,completionHandler:{ response in
                DispatchQueue.main.async {
                    if(response.count == 0){
                        controller.resetWithError(message: "Ürün bulunamadı. Tekrar deneyiniz.")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                            // Put your code which should be executed with a delay here
                        })

                    }else{
                        self.selectedProduct = response[0]
                 
                        self.performSegue(withIdentifier: "showProductSegue", sender: self)
                        controller.dismiss(animated: true, completion: nil)

                    }
                   
                    
                }
        })
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)

    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)

    }
    @IBAction func openBarcode(_ sender: Any) {
        
        let viewController = BarcodeScannerViewController()
        viewController.headerViewController.titleLabel.text = "Ürün barkodunu tara!"
        viewController.headerViewController.titleLabel.textColor = UIColor.white
       
        viewController.messageViewController.messages.notFoundText = "Ürün bulunamadı.."
        viewController.messageViewController.messages.processingText = "Ürün aranıyor.."

        viewController.messageViewController.messages.unathorizedText = "Barkod tarayıcıyı kullanabilmek için ayarlardan kameraya izin vermeniz gerekmektedir."
        viewController.messageViewController.messages.scanningText = "Barkodu kutunun içine hizalayın.\n Ürün otomatik taranacak."
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        
        present(viewController, animated: true, completion: nil)
        
      

    }
    
    @IBAction func onShopButton(_ sender: Any) {
        performSegue(withIdentifier: "oparSepetimSegue", sender: self)
    }
    func showAdvertise(){
        if let presentedViewController = self.storyboard?.instantiateViewController(withIdentifier: "AdvertisementViewController") {
            presentedViewController.providesPresentationContextTransitionStyle = true
            presentedViewController.definesPresentationContext = true
            presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            presentedViewController.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
            self.present(presentedViewController, animated: true, completion: nil)
        }
    }

    func GetSearchRequest()->SearchRequest{
        var request=SearchRequest()
        request.quickSearchText = self.quickSearchText.text!
        request.productNameText = self.productNameText.text!
        request.productCodeText = self.productCodeText.text!
        request.campaignBayiSelected = bayiKampanyaRadioButton.isSelected
        request.campaignTofasSelected = tofasKampanyaRadioButton.isSelected
        request.followProductSelected = takiptekiUrunlerRadioButton.isSelected
        request.opportunitySelected = firsatKosesiRadioButton.isSelected
        request.newProductSelected = newProductRadioButton.isSelected
        request.selectedModel = modelDropDown.selectedItem
        request.selectedMarka = markaDropDown.selectedItem
        return request
    }
    override func viewWillAppear(_ animated: Bool) {
        var basketRepo = BasketRepository()
        basketRepo.GetItemCount(completionHandler:{ response in
            self.appNotificationBarButton.badgeString = String(response)
        })
        SetNavTitle()
        
    }
    @IBAction func openMenu(_ sender: Any) {
        self.presentLeftMenuViewController(self)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    func setupDropdown(){
        
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        //        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        markaDropDown.anchorView = markaDropDownButton
        repo.GetBrandList(completionHandler:{ response in
            DispatchQueue.main.async {
                self.markaDropDown.dataSource = response
                self.markaList = response
                self.markaDropDown.reloadAllComponents()
                print("loadedd")
                
            }
        })
        modelDropDown.anchorView = modelDropDownButton
        modelDropDown.dataSource = ["Seçiniz"]
        
        markaDropDown.selectionAction = { [weak self] (index, item) in
          
            self?.selectedModel = (self?.markaList[index])!

            self?.markaDropDownButton.setTitle(item, for: .normal)
            self?.repo.GetModelList(model:(self?.selectedModel)!
                ,completionHandler:{ response in
                DispatchQueue.main.async {
                    self?.modelDropDown.dataSource = response
                    self?.modelDropDown.reloadAllComponents()

                }
            })
        }
        
        modelDropDown.selectionAction = { [weak self] (index, item) in
            self?.modelDropDownButton.setTitle(item, for: .normal)
        }

    }
    
    @IBAction func onMarkaDropDown(_ sender: Any) {
        markaDropDown.show()
    }
    
    @IBAction func onModelDropDown(_ sender: Any) {
        modelDropDown.show()

    }
    
    @IBAction func clean(_ sender: Any) {
        scroolView.setContentOffset(.zero, animated: true)
        markaDropDownButton.setTitle("Seçiniz", for: .normal)
        modelDropDownButton.setTitle("Seçiniz", for: .normal)
        //markaDropDown.dataSource = ["Seçiniz"]
        modelDropDown.dataSource = ["Seçiniz"]
        productNameText.text = ""
        productCodeText.text = ""
        quickSearchText.text = ""
        firsatKosesiRadioButton.isSelected = false
        bayiKampanyaRadioButton.isSelected = false
        tofasKampanyaRadioButton.isSelected = false
        takiptekiUrunlerRadioButton.isSelected = false
        products = [Product]()
        self.scroolView.isScrollEnabled = false

        tableView.isHidden = true
        tableView.reloadData()
    }
    @IBAction func apply(_ sender: Any) {
        
        repo.GetProductList(request:self.GetSearchRequest(),completionHandler:{ response in
            DispatchQueue.main.async {
                self.products = response
                if(self.products.count==0){
                    let tableHeight = 0
                    self.scroolView.isScrollEnabled = false
                    self.products = [Product]()
                    self.tableView.isHidden = true
                    self.tableView.reloadData()
                    let alert = UIAlertController(title: "Alert", message: "Aradığınız kriterlerde ürün bulunamadı!", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                }
                self.scroolView.isScrollEnabled = true
                
                let dataHeight = self.dataViewHeight.constant
                
                let tableHeight=self.products.count*90+40
                
                
                self.dataViewHeight.constant = dataHeight
                self.tableViewHeight.constant = CGFloat(integerLiteral: tableHeight)
                
                let totalHeight = self.dataViewHeight.constant+self.tableViewHeight.constant+50
                self.scroolViewHeight.constant = totalHeight
                self.topViewHeight.constant = totalHeight
                self.tableView.isHidden = false
                self.tableView.reloadData()
                
                
                let bottomOffset = CGPoint(x: 0, y: self.scroolView.contentSize.height - self.scroolView.bounds.size.height)
                self.scroolView.setContentOffset(bottomOffset, animated: true)
                
                self.view.layoutIfNeeded()
                self.scroolView.scrollTo(offset: self.dataView.frame.height)
       

                
            }
        })
        
    }
    
    @IBAction func onMenuButton(_ sender: Any) {
        self.presentLeftMenuViewController(self)

    }
    
    @IBAction func onBasketButton(_ sender: Any) {
        
        performSegue(withIdentifier: "sepetimSegue", sender: self)

    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showProductSegue" {
            if let toViewController = segue.destination as? ProductViewController {
                toViewController.product = self.selectedProduct
            }
        } }
}


extension SearchRestrictionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row==0){
            let cell:PointsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "tableStaticCell") as! PointsTableViewCell
            
            cell.isUserInteractionEnabled = false
            
            cell.backgroundColor =  hexStringToUIColor(hex: "22264b")
            cell.points.text=""
            cell.left.text=""

            return cell
        }
        
        var product = products[indexPath.row-1]
        let cell:SearchTableCell = self.tableView.dequeueReusableCell(withIdentifier: "searchCell2") as! SearchTableCell
        cell.layer.cornerRadius = 20
        cell.clipsToBounds = true


        cell.frame = cell.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10))
      //  cell.backgroundColor = hexStringToUIColor(hex: "F0EAD6")
        
        if(product.campaignOpt != 0){
            cell.backgroundColor = Color.campaignCellBackroundColor
            cell.campaignImage.isHidden = false
         

        }
        else if(product.NewProduct == true){
            cell.backgroundColor = Color.newProductCellBackroundColor
            cell.campaignImage.isHidden = true

        }
        else{
            cell.backgroundColor = UIColor.lightText
            cell.campaignImage.isHidden = true

        }

        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = DesignData.textColor.cgColor
        cell.idLabel.textColor = DesignData.textColor
        cell.nameLabel.textColor = DesignData.textColor
        cell.priceLabel.textColor = DesignData.textColor
        cell.idLabel.text = product.Code + (product.stillProduced ? "  (GEÇERLİ)" : "  (İPTAL)")
        cell.nameLabel.text = product.Name
        cell.priceLabel.text = product.priceWithVat + "TL  (KDV DAHİL)"
        if(product.avaibleBayi){
            cell.bayiStok.image = UIImage(named: "var")
        }else{
            cell.bayiStok.image = UIImage(named: "yok")
        }
        if(product.avaibleTofas){
            cell.tofasStok.image = UIImage(named: "var")
        }else{
            cell.tofasStok.image = UIImage(named: "yok")
        }
        return cell
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count+1
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row==0){
            return 40;
        }
        return 90.0;//Choose your custom row height
    }
    
}
extension UIScrollView {
    
    // Bonus: Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
    func scrollTo(offset:CGFloat) {
        let bottomOffset = CGPoint(x: 0, y: offset + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
    
}
extension SearchRestrictionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected")
        if(indexPath.row==0)
        {
            return 
        }
        if(!self.products[indexPath.row-1].stillProduced){
            
            HelperMethods.showAlert(controller: self, message: "Seçtiğiniz ürün geçerli değil.")
            return
        }
        
        var selectedId = self.products[indexPath.row-1].ProductId
        self.repo.GetProductById(id:selectedId!
            ,completionHandler:{ response in
                DispatchQueue.main.async {
                    self.selectedProduct = response
                    self.selectedProduct?.avaibleBayi = self.products[indexPath.row-1].avaibleBayi
                    self.selectedProduct?.avaibleTofas = self.products[indexPath.row-1].avaibleTofas
                    self.performSegue(withIdentifier: "showProductSegue", sender: self)

                }
        })
        
    }
}
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
extension UIButton {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
}
