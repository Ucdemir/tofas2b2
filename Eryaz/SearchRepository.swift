//
//  LoginRepository.cs.swift
//  Eryaz
//
//  Created by Arda Yucel on 6.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//
import  Alamofire
import Foundation

public class SearchRepository{
    func Relogin(completionHandler: @escaping (Bool) -> Void) {
        var repo=LoginRepository()
        repo.Login(username:SessionInformation.UserName,password: SessionInformation.Password,completionHandler:{ response in
            
            let data = response.object(forKey: "Data") as? NSDictionary
            if (data != nil) {
                print("token")
                SessionInformation.Token = data?.object(forKey: "Token") as! String
                completionHandler(true)
            }
            else
            {
                
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                }
                completionHandler(false)
                
            }
            
        })
    }
    func GetProductList(request:SearchRequest,completionHandler: @escaping ([Product]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.code,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Product_GetList_SearchPage",
                "Parameters": [
                    "BannerStatu": 0,
                    "CampaignProduct": request.opportunitySelected ? 1 : 0,
                    "CampaignDealer" : request.campaignBayiSelected ? 1 : 0,
                    "Code" :request.productCodeText == "" ? "*" : request.productCodeText ,
                    "Cross" :"*",
                    "DealerCode" :"",
                    "FinishIndex":24,
                    "FollowProduct": request.followProductSelected ? 1 : 0,
                    "GroupId": -1,
                    "LoginType": "Customer",
                    "MainProductCode": "*",
                    "Manufacturer": "*",
                    "Name" : request.productNameText == "" ? "*" : request.productNameText     ,
                    "NewArrival": 0,
                    "NewProduct": request.newProductSelected ? 1 : 0,
                    "Oe" :"*"    ,
                    "OnQty": 0,
                    "OnWay": 0,
                    "OrderByQuery": "",
                    "Picture": 0,
                    "ProductGroup1": (request.urunGroup1Text == nil || request.urunGroup1Text == "Seçiniz" ) ? "*" : request.urunGroup1Text,
                    "ProductGroup2": (request.urunGroup2Text == nil || request.urunGroup2Text == "Seçiniz" ) ? "*" : request.urunGroup2Text,
                    "ProductGroup3": "*",
                    "StartIndex":0,
                    "T9Text": request.quickSearchText,
                    "VehicleBrand": (request.selectedMarka == nil || request.selectedMarka == "Seçiniz" ) ? "*" : request.selectedMarka,
                    "VehicleModel": (request.selectedModel == nil || request.selectedModel == "Seçiniz" ) ? "*" : request.selectedModel,
                    "VehicleType": "*",
                    "OpportunityTofas" :request.campaignTofasSelected ? "true" : "false"
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Product_GetList_SearchPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                   
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Product]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetProductList(request:request,completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                            for item in data! {
                                
                                var product=Product()
                                product.Name = item["NameShort"] as! String
                                product.priceWithVat = item["PriceListWithVatValue"] as! String
                                product.Code = item["Code"] as! String
                                product.ProductId = item["Id"] as! Int
                                product.NewProduct = item["NewProduct"] as! Bool
                                product.minOrder = item["MinOrder"] as! Int

                                product.stillProduced = item["StillProduced"] as! Bool
                                product.avaibleBayi = (item["AvailabilityText"] as! String) == "Var" ? true : false
                                product.avaibleTofas = (item["AvailabilityTextTofas"] as! String) == "Var" ? true : false
                                 product.NewProduct = item["NewProduct"] as! Bool
                                let campaignOpt = item["Campaign_Oppt"] as! [String:Any]
                                product.campaignOpt = campaignOpt["Id"] as! Int
                                
                                product.listeFiyati = (item["PriceList"] as! [String:Any])["Value"] as! Double
                                product.listeFiyatiWithVat = (item["PriceListWithVat"] as! [String:Any])["Value"] as! Double
                                product.kdvliFiyat = (item["PriceListWithVat"] as! [String:Any])["Value"] as! Double
                                product.kdvsizFiyat = (item["PriceNet"] as! [String:Any])["Value"] as! Double
                                result.append(product)
                            }
                            
                        }
                    
                       
                      
                  
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetProductById(id:Int,completionHandler: @escaping (Product) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.code,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Product_GetItem_PriceDetail_SearchPage",
                "Parameters": [
                    "ProductId" :id
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Product_GetItem_PriceDetail_SearchPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let item = response.object(forKey: "Data") as! [String:Any]
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetProductById(id:id,completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        
                        if(item != nil) {
                            
                            
                                var product=Product()
                                product.Name = item["NameShort"] as! String
                                product.Code = item["Code"] as! String
                                product.ProductId = item["Id"] as! Int
                                product.listeFiyati = (item["PriceList"] as! [String:Any])["Value"] as! Double
                                product.listeFiyatiWithVat = (item["PriceListWithVat"] as! [String:Any])["Value"] as! Double
                                product.kdvliFiyat = (item["PriceListWithVat"] as! [String:Any])["Value"] as! Double
                                product.kdvsizFiyat = (item["PriceNet"] as! [String:Any])["Value"] as! Double
                                product.minOrder = item["MinOrder"] as! Int

                            DispatchQueue.main.async {
                                completionHandler( product)
                            }
                            
                        }
                        
                       
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetBrandList(completionHandler: @escaping ([String]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,
                                         "Data": ["Name":"VehicleBrand_GetList_SearchPage",
                                                  "Parameters":
                                                    ["Group1":"*",
                                                     "Group2":"*",
                                                     "Group3":"*",
                                                    ]
                                                ]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/VehicleBrand_GetList_SearchPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [String]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetBrandList(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                            for item in data! {
                                
                                result.append(item["Brand"] as! String
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func GetModelList(model:String, completionHandler: @escaping ([String]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,
                                         "Data": ["Name":"VehicleModel_GetList_SearchPage",
                                                  "Parameters":
                                                    [
                                                     "Brand":model,
                                                     "Group1":"*",
                                                     "Group2":"*",
                                                     "Group3":"*",
                                            ]
            ]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/VehicleModel_GetList_SearchPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [String]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetModelList(model:model,completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                            for item in data! {
                                
                                result.append(item["Model"] as! String
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetProductGroup2(group1:String,completionHandler: @escaping ([String]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": "",
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "ProductGroup2_GetList_SearchPage",
                "Parameters":
                    [
                        "ProductGroup1":group1,
                ]
                
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/ProductGroup2_GetList_SearchPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [String]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetProductGroup2(group1:group1,completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                            for item in data! {
                                
                                result.append(item["Name"] as! String)
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetProductGroup1(completionHandler: @escaping ([String]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": "",
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "ProductGroup1_GetList_SearchPage"
                
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/ProductGroup1_GetList_SearchPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [String]()
                        var message = response.object(forKey: "Message") as? String
                        if(message != nil){
                            if(message=="Validation Expried"){
                                self.Relogin(completionHandler:{ loginResponse in
                                    if(loginResponse==false){
                                        return
                                    }
                                    else{
                                        self.GetProductGroup1(completionHandler:{ response in
                                            DispatchQueue.main.async {
                                                completionHandler( response)
                                            }
                                        })                                    }
                                })
                            }
                        }
                        if(data != nil) {
                            
                            for item in data! {
                        
                                result.append(item["Name"] as! String)
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    }

