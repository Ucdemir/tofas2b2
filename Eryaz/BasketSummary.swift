//
//  BasketSummary.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 19.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

class BasketSummary{
    var toplamTutar:Double!
    var satirIskontosu:Double!
    var araTutar:Double!
    var netTutar:Double!
    var kdv:Double!
    var genelToplam:Double!
    var kazanilacakYakitPuan:Double!
    
}
