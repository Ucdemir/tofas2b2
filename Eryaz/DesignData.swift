//
//  DesignData.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class DesignData{
    static  var darkBlue = hexStringToUIColor(hex: "1D3557")
    static  var midBlue = hexStringToUIColor(hex: "457B9D")
    static  var weakBlue = hexStringToUIColor(hex: "A8DADC")
    static  var darkGrey = hexStringToUIColor(hex: "50514F")
    static  var red = hexStringToUIColor(hex: "EAE7DC")
    static  var yellow = hexStringToUIColor(hex: "FFE066")
    static  var whiteYellow = hexStringToUIColor(hex: "F1FAEE")
    static  var skinColor = hexStringToUIColor(hex: "D8C3A5")
    static var textColor2 = hexStringToUIColor(hex: "123C69")
    static var textColor = hexStringToUIColor(hex: "334431")

    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
