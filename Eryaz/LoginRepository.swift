//
//  LoginRepository.cs.swift
//  Eryaz
//
//  Created by Arda Yucel on 6.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//
import  Alamofire
import Foundation

public class LoginRepository{
   
    
    
    public func Login(username:String,password:String,completionHandler: @escaping (NSDictionary) -> Void)  {
        let parameters: [String: Any] = ["Token": "","DealerCode": "","TofasCompany": "OPAR",
                                   "Data": ["Name":"Login",
                                            "Parameters": ["Username":username, "Password":password,
                                                           "DeviceBrand":"Xiaomi", "DeviceModel":"Mi6"
                                                ,"DeviceImei": "asdasdas" ]]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
      
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Login", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.                    
                    switch response.result {
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary

                        print("logged in")
                        DispatchQueue.main.async {
                            completionHandler( response)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                    }
                    // To update anything on the main thread, just jump back on like so.
                 
            }
        )
    }
    func GetBayiListWithAdress(completionHandler: @escaping ([Bayi]) -> Void)  {
        
        let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,"Data": ["Name":"Dealer_GetList_ContactPage"]]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Dealer_GetList_ContactPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Bayi]()
                        
                        if(data != nil) {
                            
                            for item in data! {
                                var bayi = Bayi()
                                bayi.Code =  item["Code"] as! String
                                bayi.Name =  item["ShortName"] as! String
                                bayi.ShortName =  item["Name"] as! String
                               // bayi.adres1 =  item["CmpAdress1"] as! String
                               // bayi.adres2 =  item["CmpAdress2"] as! String
                                //bayi.phone =  item["CmpTel1"] as! String
                                //bayi.fax =  item["CmpFax"] as! String
                                //bayi.email =  item["CmpEmail1"] as! String

                                bayi.Id = item["Id"] as! Int
                                result.append(bayi
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var result = [Bayi]()
                        completionHandler( result)
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func GetBayiList(completionHandler: @escaping (BayiSelectionResponse) -> Void)  {
          
         
          let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
                 
         let parameters: [String: Any] = ["Token": SessionInformation.Token,"DealerCode": "","TofasCompany": SessionInformation.TofasCompany,"Data": ["Name":"Dealer_GetList_ContactPage"]]
          
          Alamofire.request("https://b2b.opar.com/Api/Mobile/Login_ListDealer", method: .post, parameters: parameters, encoding: JSONEncoding.default)
              .response(
                  queue: queue,
                  responseSerializer: DataRequest.jsonResponseSerializer(),
                  completionHandler: { response in
                      // You are now running on the concurrent `queue` you created earlier.
                      switch response.result {
                          
                          
                      case .success(let JSON):
                         // print(response)
                          print(response.data.map { String(decoding: $0, as: UTF8.self) } ?? "No data.") //this method print decodable
                         
                        
                        guard let data = response.data else { return }
                                 do {
                                     let decoder = JSONDecoder()
                                     let loginRequest = try decoder.decode(BayiSelectionResponse.self, from: data)
                                    DispatchQueue.main.async {
                                        completionHandler( loginRequest)
                                    }
                                 } catch let error {
                                     print(error)
                                    
                                 }
                              
                          
                          
                          
                          
                          
                      case .failure(let error):
                          print("Request failed with error: \(error)")
                          
                          
                      }
                      // To update anything on the main thread, just jump back on like so.
                      
              }
          )
      }
}
