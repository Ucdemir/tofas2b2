//
//  SearchViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import MGSwipeTableCell
import UIKit
import MIBadgeButton_Swift
import FOView
import DropDown
class ShoppingCartViewController : UIViewController, UITextFieldDelegate{
   
    
    @IBOutlet weak var nav: UIBarButtonItem!
    
    let pointsTableHeaderFont=UIFont(name: "HelveticaNeue-Medium", size:12);
    let encTableHeaderFont=UIFont(name: "HelveticaNeue-Light", size:13);
    let boldFont=UIFont(name: "HelveticaNeue-Bold", size:13);
    @IBOutlet weak var tableView: UITableView!
    var products=[Product]()
    var appNotificationBarButton: MIBadgeButton!      // Make it global

    var isSelected = false
    @IBAction func menuButtonAction(_ sender: Any) {
        self.presentLeftMenuViewController(self)
    }
    override func viewDidLoad() {
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .singleLine
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    
        
        
        let btn1 = UIButton(type: .custom)
        
       
        btn1.setTintWithImage(imageName: "doubleTick", tintColor: UIColor.white)
        
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(OnSelectionButton(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setTintWithImage(imageName: "refresh", tintColor: UIColor.white)

        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(OnRefreshButton(_:)), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2,item1], animated: true)
        SetNavTitle()

    }
    @objc func OnRefreshButton(_ sender: Any) {
        
        SetItems()
    }
    
    @objc func OnSelectionButton(_ sender: Any) {
        isSelected = !isSelected
        for product in self.products{
            product.IsSelected = isSelected
        }
        self.tableView.reloadData()
        
    }
    
    func SetNavTitle(){
           let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
            label.backgroundColor = UIColor.clear
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.center
            label.textColor = UIColor.white
            var bayi :String!
            if(SessionInformation.Bayi_?.code == nil){
                label.text = "ALIŞVERİŞ SEPETİ"
            }
            else{
                label.text = "ALIŞVERİŞ SEPETİ\n(\(SessionInformation.Bayi_?.name ?? ""))"
            }
            label.sizeToFit()
            label.numberOfLines = 2
            label.font = UIFont.boldSystemFont(ofSize: 14)
            self.navigationItem.titleView = label
            self.navigationItem.titleView?.setNeedsLayout()
            self.navigationItem.titleView?.setNeedsDisplay()
        
            nav.tintColor = UIColor.white
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       SetItems()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @objc func dismissKeyboard() -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
            guard NSCharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
                return false
            }
        
        return true
    }
    
    
    @IBAction func OnDevamButton(_ sender: Any) {
        var repo=BasketRepository()

        for product in products{
            if(product.IsSelected == false){
                repo.UpdateItem(product: product, isDeleted: true,completionHandler:{ response in
                    
                })
            }
        }

        self.performSegue(withIdentifier: "devamSegue", sender: self)

    }
    @IBAction func RadioAction(_ sender: Any) {
        if(self.products.count==0){
            return
        }
        let pointInTable = (sender as! UIButton).convert((sender as! UIButton).bounds.origin, to: self.tableView)
        let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        
        products[textFieldIndexPath!.row].IsSelected = !products[textFieldIndexPath!.row].IsSelected
        let indexPath = IndexPath(item: self.products.count, section: 0)
        tableView.reloadRows(at: [indexPath], with: .top)
        
    }
    @IBAction func removeAction(_ sender: Any) {
        let pointInTable = (sender as! UIButton).convert((sender as! UIButton).bounds.origin, to: self.tableView)
        let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        print(textFieldIndexPath?.row)
        
        var product = products[textFieldIndexPath!.row]
        self.products.remove(at: (textFieldIndexPath?.row)!)
        self.tableView.reloadData()
        //UPDATE PRODUCT HERE
        var repo=BasketRepository()
        repo.UpdateItem(product: product, isDeleted: true,completionHandler:{ response in
            self.SetItems()

        })
    }
    
    func GetStringFromValue(value:Int) ->String{
        var result=""
        var strValue=String(value).reversed()
        var counter=0
        for char in strValue {
            result.insert(char, at: result.startIndex)
            counter = counter+1
            if(counter%3 == 0 && strValue.count != counter){
                result.insert(".", at: result.startIndex)
            }
        }
        
        return result + ",00TL"
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let pointInTable = textField.convert(textField.bounds.origin, to: self.tableView)
             let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
             
             //UPDATE PRODUCT HERE
             self.products[textFieldIndexPath!.row].quantitiy = Int(textField.text!)
             var repo=BasketRepository()
             repo.UpdateItem(product: self.products[textFieldIndexPath!.row], isDeleted: false,completionHandler:{ response in
        
                 self.tableView.reloadData()
             })

             print(textFieldIndexPath)
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        let pointInTable = textField.convert(textField.bounds.origin, to: self.tableView)
        let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        
    }
    func GetBasketSummary()->BasketSummary{
        var toplamTutar = 0.0
        var genelToplam=0.0
        var  kdv=0.0
        var yakitPuan=0.0
        var satirIskontosu = 0.0
        var summary=BasketSummary()
        var araTutar = 0.0
        
        for product in products{
            if(product.IsSelected){
                toplamTutar += product.totalPrice
                genelToplam += product.totalCostWithVat
                kdv += product.totalVat
                yakitPuan += product.totalYakitPuan
                satirIskontosu += product.totalDiscount
                araTutar += product.totalCost
            }
        }
        
        summary.toplamTutar=toplamTutar
        summary.araTutar = araTutar
        summary.satirIskontosu = satirIskontosu
        summary.kdv = kdv
        summary.genelToplam = yakitPuan
        summary.kazanilacakYakitPuan = genelToplam
        return summary
    }
    func SetItems(){
        var repo=BasketRepository()
        repo.GetItems(completionHandler:{ response in
            
            if (response != nil) {
                self.products = response
            }
            else
            {
               self.products = [Product]()
            }
            
            self.tableView.reloadData()
            
        })
            
    }
    
}


extension ShoppingCartViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(indexPath.row == self.products.count){
            
              let cell:ShoppingCartToplamCell = self.tableView.dequeueReusableCell(withIdentifier: "shoppingCardToplamCell") as! ShoppingCartToplamCell
            var summary = GetBasketSummary()
            cell.kdvValue.text = summary.toplamTutar.withCommas()+" TL"
            cell.araTutarValue.text  = summary.araTutar.withCommas()+" TL"
            cell.genelToplamValue.text  = summary.genelToplam.withCommas()
            cell.toplamTutarValue.text  = summary.toplamTutar.withCommas()+" TL"
            cell.satirIskontosuValue.text  = summary.satirIskontosu.withCommas()+" TL"
            cell.faturaIskontosuValue.text  = "-"
            cell.kazanilacakYakitPuanValue.text  = summary.kazanilacakYakitPuan.withCommas()+" TL"
            cell.netTutarValue.text  = summary.kdv.withCommas()+" TL"
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell

        }
        else if(indexPath.row == self.products.count+1){
            
            let cell:ShoppingCartDevamCell = self.tableView.dequeueReusableCell(withIdentifier: "shoppingCardDevamCell") as! ShoppingCartDevamCell
            if(self.products.count==0){
            }
            return cell

        }
        
        var product = self.products[indexPath.row]
        let cell:ShopingCartCell = self.tableView.dequeueReusableCell(withIdentifier: "shoppingCartCell") as! ShopingCartCell
                    cell.separatorInset = UIEdgeInsets(top: 0, left: tableView.frame.width, bottom: 0, right: 0)
        cell.selectionStyle = .none

        cell.layer.borderWidth = 1
        
        cell.radioButton.isSelected = product.IsSelected
        cell.layer.borderColor = DesignData.darkGrey.cgColor
        
        cell.countTF.delegate = self
        cell.idLabel.text = String(product.Code)
        cell.nameLabel.text = product.Name
        cell.priceLabel.text = (product.listeFiyati*Double(product.quantitiy!)).withCommas()+"TL"
        var test = (product.listeFiyati*Double(product.quantitiy!))
        cell.listeFiyatLabel.text=product.listeFiyati.withCommas()+"TL"
        cell.iskontoLabel.text=product.DiscountStr
        cell.countTF.text = String(product.quantitiy!)
        
        
        cell.layer.cornerRadius = 20
        cell.clipsToBounds = true
        cell.frame = cell.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        cell.backgroundColor = Color.tableCellBackroundColor
        //configure right buttons


        cell.idLabel.font = self.boldFont
        cell.idLabel.textColor = DesignData.textColor
        cell.nameLabel.font = self.boldFont
        cell.nameLabel.textColor = DesignData.textColor
        cell.priceLabel.font = self.boldFont
        cell.priceLabel.textColor = DesignData.textColor
        cell.listeFiyatLabel.font = self.boldFont
      //  cell.listeFiyatLabel.textColor = Color.re
        cell.iskontoLabel.font = self.boldFont
        cell.iskontoLabel.textColor = DesignData.textColor
        cell.countTF.font = self.boldFont
        cell.countTF.textColor = DesignData.textColor
        
        cell.labelFiyat.textColor = Color.red
        return cell
        
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.products.count)+2
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row == self.products.count+1){
            
            return 60
        }
        else if(indexPath.row == self.products.count){
            return 280
        }
        
        return 150
    }
    
}

extension ShoppingCartViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
extension Double {
    
    
    func withCommas() -> String {
    
        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        var string =  numberFormatter.string(from: NSNumber(value:self))!
        var result = ""
        for char in string {
            if(char == ","){
                result.append(".")
            }
            else if ( char == "."){
                result.append(",")
            }
            else{
                result.append(char)
            }
        }
        return result
    }
}
