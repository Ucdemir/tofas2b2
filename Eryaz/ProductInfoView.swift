//
//  ProductInfoView.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
class ProductInfoView:UIView{
    
    @IBOutlet weak var nameLabel:UILabel?
    @IBOutlet weak var codeLabel:UILabel?
    @IBOutlet weak var priceLabel:UILabel?
}
