//
//  LoginController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 20/06/2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit


import Alamofire
import SWXMLHash
import StringExtensionHTML
import AEXML
class LoginManager: NSObject{
    
    /*
    
    static let deviceID = UIDevice.current.identifierForVendor!.uuidString
    var controller:LoginViewController!
    
    init(controller:LoginViewController)
    {
        
        self.controller=controller;
    }
    func checkCustomerLogin(_ username:String,password:String ,completion: @escaping (Bool) -> Void){
        
        let soapRequest = AEXMLDocument()
        let attributes = ["xmlns:SOAP-ENV" : "http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns1" : "http://tempuri.org/"]
        let envelope = soapRequest.addChild(name: "SOAP-ENV:Envelope", attributes: attributes)
        let body = envelope.addChild(name: "SOAP-ENV:Body")
        let getSalesmanLogin = body.addChild(name: "ns1:GetCustomerLogin")
        getSalesmanLogin.addChild(name: "ns1:pCode", value: username)
        getSalesmanLogin.addChild(name: "ns1:pPassword", value: password)
        getSalesmanLogin.addChild(name: "ns1:pSystemType", value: "0")
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = URL(string: "http://b2b.basaran-otomotiv.com.tr/MobileWebService.asmx")
        var mutableR = URLRequest(url: theURL! as URL )
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.addValue("http://tempuri.org/GetCustomerLogin", forHTTPHeaderField: "SOAPAction")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    let xml = SWXMLHash.parse(xmlString)
                    let body =  xml["soap:Envelope"]["soap:Body"]
                    if let countriesElement = body["GetCustomerLoginResponse"]["GetCustomerLoginResult"].element {
                        let getCountriesResult = countriesElement.text!
                        
                        print(getCountriesResult)
                        if(getCountriesResult=="null"){
                            completion(false)
                        }
                        else
                        {
                            let dict = self.convertToDictionary(text: getCountriesResult)
                            Session.customer=Customer(id: dict!["Id"]! as! Int, code: dict!["Code"]! as! String, name: dict!["Name"]! as! String)
                            completion(true)
                        }
                    }
                }else{
                    print("error fetching XML")
                }
                
        }
    }

    
    func checkSalesmanLogin(_ username:String,password:String ,completion: @escaping (Bool) -> Void){
 
        let soapRequest = AEXMLDocument()
        let attributes = ["xmlns:SOAP-ENV" : "http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns1" : "http://tempuri.org/"]
        let envelope = soapRequest.addChild(name: "SOAP-ENV:Envelope", attributes: attributes)
        let body = envelope.addChild(name: "SOAP-ENV:Body")
        let getSalesmanLogin = body.addChild(name: "ns1:GetSalesmanLogin")
        getSalesmanLogin.addChild(name: "ns1:pCode", value: username)
        getSalesmanLogin.addChild(name: "ns1:pPassword", value: password)
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = URL(string: "http://b2b.basaran-otomotiv.com.tr/MobileWebService.asmx")
        var mutableR = URLRequest(url: theURL! as URL )
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.addValue("http://tempuri.org/GetSalesmanLogin", forHTTPHeaderField: "SOAPAction")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    let xml = SWXMLHash.parse(xmlString)
                    let body =  xml["soap:Envelope"]["soap:Body"]
                    if let countriesElement = body["GetSalesmanLoginResponse"]["GetSalesmanLoginResult"].element {
                        let getCountriesResult = countriesElement.text!
                        
                        if(getCountriesResult=="null"){
                           completion(false)
                        }
                        else
                        {
                            let dict = self.convertToDictionary(text: getCountriesResult)
                            Session.salesman=Salesman(id: dict!["Id"]! as! Int, code: dict!["Code"]! as! String, name: dict!["Name"]! as! String, customerType: dict!["CustomerType"]! as! Bool)
                            completion(true)
                        }
                    }
                }else{
                    print("error fetching XML")
                }
                
        }
}
 
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
        
   */
    
}
