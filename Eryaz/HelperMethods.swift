//
//  HelperMethods.swift
//  Eryaz
//
//  Created by Ferit Özcan on 20/06/2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit

class HelperMethods{
    
    static func printDictionaryAsJson(){

    }
    static func showAlert(controller:UIViewController,message:String){
        let alert = UIAlertController(title: "Uyarı!", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        controller.present(alert, animated: true, completion: nil)
    }
}
