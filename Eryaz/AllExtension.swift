//
//  AllExtension.swift
//  Eryaz
//
//  Created by Omer Demirci on 4.06.2020.
//  Copyright © 2020 EryazSoftware. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift
extension UIImage {

    public static func loadFrom(url: URL, completion: @escaping (_ image: UIImage?) -> ()) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    completion(UIImage(data: data))
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }

}

extension UIColor{
    convenience init(rgb: UInt, alphaVal: CGFloat) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: alphaVal
        )
    }
}


extension UITextField {
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }
}


extension MIBadgeButton {

    func setColorWithImage(imageName : String , tintColor: UIColor) {
       
        let image = UIImage(named:imageName )
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)

        self.setImage(tintedImage, for: .normal)
        self.tintColor =  tintColor
   }

}
extension UIButton {

    func setTintWithImage(imageName : String , tintColor: UIColor) {
       
        let image = UIImage(named:imageName )
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)

        self.setImage(tintedImage, for: .normal)
        self.tintColor =  tintColor
   }

}


extension UIImageView {
  func imageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}

extension Double{
    
    
    func priceFormatFixer()-> String{
        
        //NSNumberFormatter().numberFromString("55")!.decimalValue
        
        
        
        let doubleFormattedPrice = Double(String(format:"%.02f", self))
        
        var realText = NumberFormatter.localizedString(from: NSNumber(value: doubleFormattedPrice!), number: NumberFormatter.Style.decimal)


         //NumberFormatter.localizedString(from: NSNumber(price)), number: NumberFormatter.Style.decimal)

        realText = realText.replace(target: ".", withString:"?")
        realText = realText.replace(target: ",", withString:".")
        
        realText = realText.replace(target: "?", withString:",")
        
      
        
        
        let count = realText.count
        let z  = realText[count-2]
        
        if z == ","{
            
            realText = realText + "0"
        }
        
        
        return realText.replace(target: "?", withString:",")+"TL"
    }
    
   
}

extension String{
    
    func replace(target: String, withString: String) -> String
       {
           return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
       }
    
    
    var length: Int {
         return count
     }

     subscript (i: Int) -> String {
         return self[i ..< i + 1]
     }

     func substring(fromIndex: Int) -> String {
         return self[min(fromIndex, length) ..< length]
     }

     func substring(toIndex: Int) -> String {
         return self[0 ..< max(0, toIndex)]
     }

     subscript (r: Range<Int>) -> String {
         let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                             upper: min(length, max(0, r.upperBound))))
         let start = index(startIndex, offsetBy: range.lowerBound)
         let end = index(start, offsetBy: range.upperBound - range.lowerBound)
         return String(self[start ..< end])
     }
}
