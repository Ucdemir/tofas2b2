//
//  BayiListSelection.swift
//  Eryaz
//
//  Created by Omer Demirci on 13.06.2020.
//  Copyright © 2020 EryazSoftware. All rights reserved.
//

import Foundation


struct BayiSelectionResponse: Codable {
    let result: Int
    let message: String
    let data: [BayiSelectionData]

    enum CodingKeys: String, CodingKey {
        case result = "Result"
        case message = "Message"
        case data = "Data"
    }
}


struct BayiSelectionData: Codable {
    internal init(id: Int, code: String, name: String, email: String, imageBase: JSONNull?, picturePath: JSONNull?, regionOpar: String, regionOparCode: String, isOparDealer: Bool, regionMarelli: String, regionMarelliCode: String, isMarelliDealer: Bool, tel1: String, tel2: String, shortName: String, logoPath: String, address: String, cmpCode: JSONNull?, cmpAdress1: JSONNull?, cmpAdress2: JSONNull?, cmpCity: JSONNull?, cmpTel1: JSONNull?, cmpTel2: JSONNull?, cmpFax: JSONNull?, cmpContactPerson: JSONNull?, cmpEmail1: JSONNull?, cmpEmail2: JSONNull?, cmpWebAdress: JSONNull?, cmpTaxOffice: JSONNull?, cmpTaxNumber: JSONNull?, campaignPassword: JSONNull?, paymentPassword: JSONNull?, rulePassword: JSONNull?, isHub: Bool, hubID: Int, isBranch: Bool, allowSellAlfChrLnc: Bool, area: Int, dealerRequest: DealerRequest, bayi: JSONNull?, accountingMailAddresses: JSONNull?, showQuantity: Bool, idForPayment: Int, createDate: CreateDateEnum, createID: Int, editDate: CreateDateEnum, editID: Int, deleted: Bool) {
        self.id = id
        self.code = code
        self.name = name
        self.email = email
        self.imageBase = imageBase
        self.picturePath = picturePath
        self.regionOpar = regionOpar
        self.regionOparCode = regionOparCode
        self.isOparDealer = isOparDealer
        self.regionMarelli = regionMarelli
        self.regionMarelliCode = regionMarelliCode
        self.isMarelliDealer = isMarelliDealer
        self.tel1 = tel1
        self.tel2 = tel2
        self.shortName = shortName
        self.logoPath = logoPath
        self.address = address
        self.cmpCode = cmpCode
        self.cmpAdress1 = cmpAdress1
        self.cmpAdress2 = cmpAdress2
        self.cmpCity = cmpCity
        self.cmpTel1 = cmpTel1
        self.cmpTel2 = cmpTel2
        self.cmpFax = cmpFax
        self.cmpContactPerson = cmpContactPerson
        self.cmpEmail1 = cmpEmail1
        self.cmpEmail2 = cmpEmail2
        self.cmpWebAdress = cmpWebAdress
        self.cmpTaxOffice = cmpTaxOffice
        self.cmpTaxNumber = cmpTaxNumber
        self.campaignPassword = campaignPassword
        self.paymentPassword = paymentPassword
        self.rulePassword = rulePassword
        self.isHub = isHub
        self.hubID = hubID
        self.isBranch = isBranch
        self.allowSellAlfChrLnc = allowSellAlfChrLnc
        self.area = area
        self.dealerRequest = dealerRequest
        self.bayi = bayi
        self.accountingMailAddresses = accountingMailAddresses
        self.showQuantity = showQuantity
        self.idForPayment = idForPayment
        self.createDate = createDate
        self.createID = createID
        self.editDate = editDate
        self.editID = editID
        self.deleted = deleted
    }
    
    
 
    
    let id: Int
    let code, name, email: String
    let imageBase, picturePath: JSONNull?
    let regionOpar, regionOparCode: String
    let isOparDealer: Bool
    let regionMarelli, regionMarelliCode: String
    let isMarelliDealer: Bool
    let tel1, tel2, shortName, logoPath: String
    let address: String
    let cmpCode, cmpAdress1, cmpAdress2, cmpCity: JSONNull?
    let cmpTel1, cmpTel2, cmpFax, cmpContactPerson: JSONNull?
    let cmpEmail1, cmpEmail2, cmpWebAdress, cmpTaxOffice: JSONNull?
    let cmpTaxNumber, campaignPassword, paymentPassword, rulePassword: JSONNull?
    let isHub: Bool
    let hubID: Int
    let isBranch, allowSellAlfChrLnc: Bool
    let area: Int
    let dealerRequest: DealerRequest
    let bayi, accountingMailAddresses: JSONNull?
    let showQuantity: Bool
    let idForPayment: Int
    let createDate: CreateDateEnum
    let createID: Int
    let editDate: CreateDateEnum
    let editID: Int
    let deleted: Bool

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case code = "Code"
        case name = "Name"
        case email = "Email"
        case imageBase = "ImageBase"
        case picturePath = "PicturePath"
        case regionOpar = "RegionOpar"
        case regionOparCode = "RegionOparCode"
        case isOparDealer = "IsOparDealer"
        case regionMarelli = "RegionMarelli"
        case regionMarelliCode = "RegionMarelliCode"
        case isMarelliDealer = "IsMarelliDealer"
        case tel1 = "Tel1"
        case tel2 = "Tel2"
        case shortName = "ShortName"
        case logoPath = "LogoPath"
        case address = "Address"
        case cmpCode = "CmpCode"
        case cmpAdress1 = "CmpAdress1"
        case cmpAdress2 = "CmpAdress2"
        case cmpCity = "CmpCity"
        case cmpTel1 = "CmpTel1"
        case cmpTel2 = "CmpTel2"
        case cmpFax = "CmpFax"
        case cmpContactPerson = "CmpContactPerson"
        case cmpEmail1 = "CmpEmail1"
        case cmpEmail2 = "CmpEmail2"
        case cmpWebAdress = "CmpWebAdress"
        case cmpTaxOffice = "CmpTaxOffice"
        case cmpTaxNumber = "CmpTaxNumber"
        case campaignPassword = "CampaignPassword"
        case paymentPassword = "PaymentPassword"
        case rulePassword = "RulePassword"
        case isHub = "IsHub"
        case hubID = "HubId"
        case isBranch = "IsBranch"
        case allowSellAlfChrLnc = "AllowSellAlfChrLnc"
        case area = "Area"
        case dealerRequest = "DealerRequest"
        case bayi
        case accountingMailAddresses = "AccountingMailAddresses"
        case showQuantity = "ShowQuantity"
        case idForPayment = "IdForPayment"
        case createDate = "CreateDate"
        case createID = "CreateId"
        case editDate = "EditDate"
        case editID = "EditId"
        case deleted = "Deleted"
    }
}

// MARK: - DealerRequest
struct DealerRequest: Codable {
    let id, customerID: Int
    let customer, customerCode: JSONNull?
    let dealerID: Int
    let dealer, dealerCode, note, cancelNote: JSONNull?
    let dealerNote: JSONNull?
    let status: Int
    let statusStr: StatusStr
    let createDate: CreateDateEnum
    let createID: Int
    let editDate: CreateDateEnum
    let editID: Int
    let deleted: Bool

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case customerID = "CustomerId"
        case customer = "Customer"
        case customerCode = "CustomerCode"
        case dealerID = "DealerId"
        case dealer = "Dealer"
        case dealerCode = "DealerCode"
        case note = "Note"
        case cancelNote = "CancelNote"
        case dealerNote = "DealerNote"
        case status = "Status"
        case statusStr = "StatusStr"
        case createDate = "CreateDate"
        case createID = "CreateId"
        case editDate = "EditDate"
        case editID = "EditId"
        case deleted = "Deleted"
    }
}

enum StatusStr: String, Codable {
    case başvurunuzAlındı = "Başvurunuz Alındı"
}

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}


enum CreateDateEnum: String, Codable {
    case the00010101T000000 = "0001-01-01T00:00:00"
}
