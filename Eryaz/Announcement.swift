//
//  Announcement.swift
//  Eryaz
//
//  Created by Arda Yucel on 6.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

class Announcement{
    var Id:Int!
    var Path:String!
    var Description:String!
    var Header : String!
    var Code : String!
    var Price : String!
}
