import UIKit
class CustomDrawView: UIView {
    
  
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        let width=frame.width/8
        let height=frame.height/12
        let theImageView = UIImageView(frame: CGRect(x: frame.width/2-width/2, y: height, width: width, height: height))
        var image :UIImage!
        if(SessionInformation.TofasCompany == "OPAR"){
            image=UIImage(named: "opar_logo_white")
        }
        else{
            image=UIImage(named: "magneti")
            
        }
        theImageView.image = image
        theImageView.translatesAutoresizingMaskIntoConstraints = false //You need to call this property so the image is added to your view
        addSubview(theImageView)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        var image :UIImage!
        if(SessionInformation.TofasCompany == "OPAR"){
            image=UIImage(named: "opar_logo_white")
        }
        else{
            image=UIImage(named: "magneti_white")
        }
        setup()
        let width=self.frame.width/3
        let height=self.frame.height/4
        let theImageView = UIImageView(frame: CGRect(x:self.frame.width/2-width/2 , y: self.frame.height/2-height/2, width: width, height: height))
        theImageView.contentMode = UIView.ContentMode.scaleAspectFill
        theImageView.image = image
        addSubview(theImageView)
        backgroundColor = UIColor.clear
    }
  
    func setup() {
        
        // Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        // The Bezier path that we made needs to be converted to
        // a CGPath before it can be used on a layer.
        shapeLayer.path = createBezierPath().cgPath
        // apply other properties related to the path
        shapeLayer.strokeColor = DesignData.darkBlue.cgColor
        shapeLayer.fillColor = DesignData.darkBlue.cgColor
        shapeLayer.lineWidth = 1.0
        shapeLayer.position = CGPoint(x: 0, y: 0)
        // add the new layer to our custom view
        self.layer.addSublayer(shapeLayer)
    }
    
    func createBezierPath() -> UIBezierPath {
        
       let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: frame.size.height))
        path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: frame.size.height*7/9))
        path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
        path.close()
        return path
        // see previous code for creating the Bezier path
    }
}
