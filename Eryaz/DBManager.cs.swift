//
//  DBManager.swift
//  Eryaz
//
//  Created by Ferit Özcan on 22/06/2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import CoreData
import Foundation
class DBManager{
    
    
    static func insertData(bayiKodu:String,username:String,password:String,type:Bool,finger_print_option:Int,remembered:Bool){
        let user=USER(context: context)
        user.username=username
        user.touch_id_option = Int32(finger_print_option)
        user.bayiKodu=bayiKodu
        user.password=password
        user.remembered=remembered
        user.type=type
        appDelegate.saveContext()
        print("INSERTED")
    }
    
    static func fetchData()->[USER]
    {
        var data=[USER]()
        
        do{
            
            
            data = try context.fetch(USER.fetchRequest())
            
        }catch{
            //handlee error
        }
        
        return data
    }
    
    static func setLastLoginToDB(bayiKodu:String,username:String,password:String,type:Bool,finger_print_option:Int,remembered:Bool){
        var data = fetchData()
        if(data.count==0)
        {
            insertData(bayiKodu:bayiKodu,username: username, password: password,type:type, finger_print_option: finger_print_option,remembered:remembered)        }
        else if(data.count==1){
            let user=data.popLast()
            if(user?.password==password&&user?.username==username&&user?.type==type)
            {
                return;
            }
            else{
              //  deleteData()
                insertData(bayiKodu:bayiKodu,username: username, password: password,type:type, finger_print_option: finger_print_option,remembered:remembered )
            }
        }
        else{
            //deleteData()
            insertData(bayiKodu:bayiKodu,username: username, password: password,type:type, finger_print_option: finger_print_option,remembered:remembered )        }
    }
    static func deleteData() {
        
        let fetchRequest: NSFetchRequest<USER> = USER.fetchRequest()
        let deleteRequest=NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult> )
        
        do{
            print("deleting all")
            try context.execute(deleteRequest)
        }catch{
            print(error.localizedDescription)
        }
    }
    
    
    
}
