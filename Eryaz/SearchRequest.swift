//
//  SearchRequest.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 12.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//

class SearchRequest{
    var opportunitySelected:Bool!
    var followProductSelected:Bool!
    var newProductSelected:Bool!
    var campaignTofasSelected:Bool!
    var campaignBayiSelected:Bool!
    var selectedMarka:String!
    var selectedModel:String!
    var searchText:String!
    var quickSearchText: String!
    var productNameText: String!
    var productCodeText: String!
    var urunGroup1Text:String?
    var urunGroup2Text:String?
}
