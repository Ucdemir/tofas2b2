//
//  SearchTableCell.swift
//  Eryaz
//
//  Created by Ferit Özcan on 27.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import UICheckbox_Swift

class ShopingCartCell: MGSwipeTableCell {
    
    @IBOutlet weak var radioButton: UICheckbox!

    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countTF: UITextField!
    @IBOutlet weak var listeFiyatLabel: UILabel!
    @IBOutlet weak var labelFiyat: UILabel!
    @IBOutlet weak var iskontoLabel: UILabel!

    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 1
            frame.size.height -= 2 * 2
            super.frame = frame
        }
    }
    
    
    override func awakeFromNib() {
              super.awakeFromNib()
             
           
       
           
          }
}
