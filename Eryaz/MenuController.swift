//
//  MenuController.swift
//  Example
//
//  Created by Teodor Patras on 16/06/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import UIKit

class MenuController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var markaButon: UIButton!
    
    @IBOutlet weak var sideMenuFooterView: SideMenuFooterView!
    
    @IBOutlet weak var tableView: UITableView!
    let font=UIFont(name: "HelveticaNeue-Light", size:13);

    let segues = ["showCenterController1", "showCenterController2", "showCenterController3"]
    private var previousIndex: NSIndexPath?
    
    var selectedBayi:BayiSelectionData?
    @IBAction func changeMarka(_ sender:Any){
        if(SessionInformation.TofasCompany == "MARELLI"){
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "oparViewController") as! OparViewController
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
            
            self.sideMenuViewController!.hideMenuViewController()
            SessionInformation.TofasCompany = "OPAR"
            markaButon.setImage(UIImage(named: "opar_logo_blue"), for: .normal)


        }
        else{
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "marelliViewController") as! MagnetiViewController
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
            
            self.sideMenuViewController!.hideMenuViewController()
            SessionInformation.TofasCompany = "MARELLI"
            markaButon.setImage(UIImage(named: "magneti"), for: .normal)

            
        }
    }
    @IBAction func changeBayi(_ sender: Any) {
        if(SessionInformation.TofasCompany == "OPAR"){
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "oparViewController") as! OparViewController
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
            
            self.sideMenuViewController!.hideMenuViewController()
            controller.changeBayi()
            SessionInformation.Bayi_ = nil
            controller.SetNavTitle()

        }
        else{
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "marelliViewController") as! MagnetiViewController
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
            
            self.sideMenuViewController!.hideMenuViewController()
            controller.changeBayi()
            SessionInformation.Bayi_ = nil
            controller.SetNavTitle()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedBayi = SessionInformation.Bayi_
  
        tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor(rgb: 0xb9b9b9, alphaVal: 1.0)
       // tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 160);
        

    }
    override func viewWillAppear(_ animated: Bool) {
        if(self.selectedBayi == nil || self.selectedBayi?.code != SessionInformation.Bayi_?.code){
            self.selectedBayi = SessionInformation.Bayi_
            tableView.reloadData()
        }
        if(SessionInformation.TofasCompany == "OPAR"){
            markaButon.setImage(UIImage(named: "opar_logo_blue"), for: .normal)

        }else{
            markaButon.setImage(UIImage(named: "magneti"), for: .normal)

        }
    }
  
    @IBAction func onFacebookButton(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "fb://profile/OparParcaVeAksesuar")!) {
            UIApplication.shared.open(URL(string: "fb://profile/OparParcaVeAksesuar")!, options: [:])
        } else {
            UIApplication.shared.open(URL(string: "https://facebook.com/OparParcaVeAksesuar")!, options: [:])
        }
    }
    @IBAction func onInstagramButton(_ sender: Any) {
        var instagramAppURL = URL(string: "instagram://user?username=opar_parcaveaksesuar")
        if UIApplication.shared.canOpenURL(instagramAppURL!) {
            UIApplication.shared.openURL(instagramAppURL!)
        }else{
            guard let url = URL(string: "https://www.instagram.com/opar_parcaveaksesuar") else { return }
            UIApplication.shared.openURL(url)
        }


    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row==0)
        {
            return 160
        }

         if(indexPath.row==1)
        {
            return 110
        }
        if(indexPath.row==9)
        {
            return 55
        }
      
        return 35.0;//Choose your custom row height
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
         func tableView(_ tableView: UITableView,
                                cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if(indexPath.row==0)
            {
                 let cell:SideMenuLogoCell = self.tableView.dequeueReusableCell(withIdentifier: "menuLogoCell") as! SideMenuLogoCell
                
                    cell.customImageView?.backgroundColor = DesignData.darkBlue
                    cell.backgroundColor = DesignData.darkBlue
                    cell.setCustomImage(image: #imageLiteral(resourceName: "opar_logo_white"))
                
                return cell
            }
            else if(indexPath.row==1)
            {
                let cell:SideMenuUserCell = self.tableView.dequeueReusableCell(withIdentifier: "menuUserCell") as! SideMenuUserCell
                
                cell.customImageView?.backgroundColor = DesignData.darkBlue
                cell.titleLabel.text = SessionInformation.Bayi_?.name
                cell.usernameLabel.text = SessionInformation.username
                //cell.usernameLabel.textColor=DesignData.darkBlue
                cell.titleLabel.textColor=DesignData.darkBlue
                //cell.titleLabel.font=font
            //    cell.usernameLabel.font=font
                
                cell.backgroundColor = UIColor.white
                

                cell.setCustomImage(image: UIImage(named: "noimg")!)
                
                return cell
            }
            else if(indexPath.row==9)
            {
                let cell:SideMenuSocialCell = self.tableView.dequeueReusableCell(withIdentifier: "sideMenuSocialCell") as! SideMenuSocialCell
                return cell
            }
           
            else
            {
                let cell:SideMenuTextCell = self.tableView.dequeueReusableCell(withIdentifier: "menuTextCell") as! SideMenuTextCell
                cell.label?.textColor = UIColor(rgb: 0x313941, alphaVal: 1.0)
                // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
          
                if(indexPath.row==2){
                    cell.label!.text = "ANASAYFA"
                }
                if(indexPath.row==3){
                    cell.label!.text = "ARAMA"
                }
                if(indexPath.row==4){
                    cell.label!.text = "SEPETİM"
                }
                if(indexPath.row==5){
                    cell.label!.text = "FIRSAT KÖŞESİ"
                }
                if(indexPath.row==6){
                    cell.label!.text = "HIZLI SİPARİŞ"
                }
                if(indexPath.row==7){
                    cell.label!.text = "İLETİŞİM"
                }
                if(indexPath.row==8){
                    cell.label!.text = "ÇIKIŞ YAP"
                }
                
                
                
                return cell
            }
            
       
    }
    
         func tableView(_ tableView: UITableView,
                                didSelectRowAt indexPath: IndexPath)  {
            
            //anasayfa
            if(indexPath.row==2){
               
                let controller = SessionInformation.TofasCompany == "OPAR" ? "oparViewController" : "marelliViewController"; self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: controller)), animated: true)
                
                self.sideMenuViewController!.hideMenuViewController()
            }
            if(indexPath.row==3){
                let controller = SessionInformation.TofasCompany == "OPAR" ? "searchViewController" : "marelliSearchViewController";
                self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: controller)), animated: true)
                
                self.sideMenuViewController!.hideMenuViewController()


                //homeVc?.performSegue(withIdentifier: "searchSegue2", sender: self)

            }
            if(indexPath.row==4){
              
                if(SessionInformation.TofasCompany == "OPAR"){
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "oparViewController") as! OparViewController
                    self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                    
                    self.sideMenuViewController!.hideMenuViewController()
                    controller.onShopButton(self)
                    
                }
                else{
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "marelliViewController") as! MagnetiViewController
                    self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                    
                    self.sideMenuViewController!.hideMenuViewController()
                    controller.openSepetim()
                }
                
               

                
                //homeVc?.performSegue(withIdentifier: "searchSegue2", sender: self)
                
            }
            if(indexPath.row==5){
                
                                 if(SessionInformation.TofasCompany == "OPAR"){
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "oparViewController") as! OparViewController
                    self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                    
                    self.sideMenuViewController!.hideMenuViewController()
                    controller.onFirsatKosesi()
                    
                }
                else{
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "marelliViewController") as! MagnetiViewController
                    self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                    
                    self.sideMenuViewController!.hideMenuViewController()
                    controller.onFirsatKosesi()
                }
                
            }
            if(indexPath.row==4){
                print("ok")
                var repo = LoginRepository()
            
                
                
            }
            
            if(indexPath.row==5){
            }
            if(indexPath.row==6){
                if(SessionInformation.TofasCompany == "OPAR"){
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "oparViewController") as! OparViewController
                    self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                    
                    self.sideMenuViewController!.hideMenuViewController()
                    controller.quickOrder()
                    
                }
                else{
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "marelliViewController") as! MagnetiViewController
                    self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                    
                    self.sideMenuViewController!.hideMenuViewController()
                    controller.quickOrder()
                }
            }
            if(indexPath.row==7){
                let controller = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
                self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: controller), animated: true)
                
                self.sideMenuViewController!.hideMenuViewController()
            }
            if(indexPath.row==8){
                print("7")
                DBManager.deleteData()
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                }
                
              //  self.sideMenuViewController!.hideMenuViewController()
                
         }
            
        if let index = previousIndex {
            tableView.deselectRow(at: index as IndexPath, animated: true)
        }
        
        previousIndex = indexPath as NSIndexPath?
    }
    
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
