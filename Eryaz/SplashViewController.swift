//
//  SplashViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 28.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import RevealingSplashView

class SplashViewController:UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize a revealing Splash with with the iconImage, the initial size and the background color
        var image :UIImage!
        if(SessionInformation.TofasCompany == "OPAR"){
            image=UIImage(named: "opar_logo_white")
        }
        else{
            image=UIImage(named: "magneti_white")

        }
        let revealingSplashView = RevealingSplashView(iconImage: image,iconInitialSize: CGSize(width: 150, height: 150), backgroundColor: DesignData.darkBlue)
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        
        //Adds the revealing splash view as a sub view
        self.view.addSubview(revealingSplashView)
        
        //Starts animation
        revealingSplashView.startAnimation(){
            print("Completed")
        }
        
    }
}
