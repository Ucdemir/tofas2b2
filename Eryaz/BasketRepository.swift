//
//  BasketRepository.swift
//  Eryaz
//
//  Created by Ferit Ozcan on 12.01.2019.
//  Copyright © 2019 EryazSoftware. All rights reserved.
//
import  Alamofire
import Foundation

class BasketRepository{
    func GetItemCount(completionHandler: @escaping (Int) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.id,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_GetCount",
                "Parameters": [
                    "DealerId": SessionInformation.Bayi_?.id
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_GetCount", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? Int
                        
                        if(data != nil) {
                            DispatchQueue.main.async {
                                completionHandler( data!)
                            }
             
                        }
                        else{
                            DispatchQueue.main.async {
                                completionHandler( 0)
                            }
                        }
                        
                      
                        
                        
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }              }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetItems(completionHandler: @escaping ([Product]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.id,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_GetList",
                "Parameters": [
                    "DealerId": SessionInformation.Bayi_?.id
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_GetList", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        print(response.data.map { String(decoding: $0, as: UTF8.self) } ?? "No data.")
                        
                        
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [Product]()
                        
                        if(data != nil) {
                            
                            for item in data! {
                                var product=Product()
                                product.quantitiy = item["Quantity"] as! Int

                                product.ProductId = item["Id"] as! Int
                                var item2 = item["Product"] as! [String:Any]
                                product.Name = item2["NameShort"] as! String
                                product.Code = item2["Code"] as! String

                                product.listeFiyati = (item2["PriceList"] as! [String:Any])["Value"] as! Double
                                product.listeFiyatiWithVat = (item2["PriceListWithVat"] as! [String:Any])["Value"] as! Double
                                product.kdvliFiyat = (item2["PriceListWithVat"] as! [String:Any])["Value"] as! Double
                                product.kdvsizFiyat = (item2["PriceNet"] as! [String:Any])["Value"] as! Double

                                product.DiscountStr = item["DiscountStr"] as! String
                                product.totalVat = item["TotalVAT"] as! Double
                                product.totalPrice = item["TotalPrice"] as! Double
                                product.totalYakitPuan = item["TotalYakitPuan"] as! Double
                                product.totalCostWithVat = item["TotalCostWithVAT"] as! Double
                                product.totalCost = item["TotalCost"] as! Double
                                product.totalDiscount = item["TotalDiscount"] as! Double
                                result.append(product
                                )
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }                }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func QuickOrder(items:[[String: AnyObject]],completionHandler: @escaping (Bool) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.code,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Product_Order_Quick_OrderPage",
                "Parameters": [
                    "DealerId": SessionInformation.Bayi_?.id,
                    "OrderList": items
                ]
            ]
        ]
        do{
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: parameters,
                options: [.prettyPrinted]) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .ascii)
                print("JSON string = \(theJSONText!)")
            }
    } catch {
    print(error.localizedDescription)
    }
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Product_Order_Quick_OrderPage", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let message = response.object(forKey: "Message") as? String
                        
                        if(message != nil) {
                            DispatchQueue.main.async {
                                completionHandler( message=="Success" ? true : false)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                completionHandler( false)
                            }
                        }
                        
                        
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }                 }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func AddItem(quantitiy:Int,productCode:String,productId:Int,completionHandler: @escaping (Bool) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.code,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_AddItem",
                "Parameters": [
                    "DealerId": SessionInformation.Bayi_?.id,
                    "ProductId": productId,
                    "Index": 0,
                    "Quantity" : quantitiy,
                    "ProductCode" :productCode
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_AddItem", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let message = response.object(forKey: "Message") as? String
                        
                        if(message != nil) {
                            DispatchQueue.main.async {
                                completionHandler( message=="Success" ? true : false)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                completionHandler( false)
                            }
                        }
                        
                       
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }                 }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    func GetShippingAdress(completionHandler: @escaping ([ShippingInfo]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.id,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_GetList_ShippingAddress",
                "Parameters": [
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_GetList_ShippingAddress", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [ShippingInfo]()
                        
                        if(data != nil) {
                            
                            for item in data! {
                                var info = ShippingInfo()
                                info.id = item["id"] as! Int
                                info.text = item["teslimAdresi"] as! String
                                  result.append(info
                                )
                            }
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }                  }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func GetShippingNote(completionHandler: @escaping ([ShippingInfo]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.id,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_GetList_ShippingNote",
                "Parameters": [
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_GetList_ShippingNote", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [ShippingInfo]()
                        
                        if(data != nil) {
                            
                            for item in data! {
                                var info = ShippingInfo()
                                info.id = item["Id"] as! Int
                                info.text = item["Note"] as? String
                                result.append(info
                                )
                            }
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }                  }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func ApproveOrder(shipmentValue:Int,shipmentAddressValue:Int,note:String,completionHandler: @escaping (Bool) -> Void){
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.code,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_Approve",
                "Parameters": [
                    "DealerId":SessionInformation.Bayi_?.code,
                    "Note":note,
                    "ShipmentValue":shipmentValue,
                    "ShipmentAddressValue":shipmentAddressValue
                ]
            ]
        ]
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: parameters,
            options: [.prettyPrinted]) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")}
            
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_Approve", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        DispatchQueue.main.async {
                            completionHandler( true)
                        }
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            DispatchQueue.main.async {
                                completionHandler( false)
                            }
                        }
                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func UpdateItem(product:Product,isDeleted:Bool,completionHandler: @escaping ([Void]) -> Void){
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.code,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_UpdateItem",
                "Parameters": [
                    "Id":product.ProductId,
                    "Quantity":product.quantitiy,
                    "DiscSpecial":0,
                    "Status":isDeleted ? 2 : 0,
                    "OrderId":-1,
                    "Checked":false,
                    "Notes":"",
                    "IsFixedPrice":false,
                    "FixedPrice":0,
                    "FixedCurrency":0,
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_UpdateItem", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                                
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }  }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
    
    func GetShippingType(completionHandler: @escaping ([ShippingInfo]) -> Void)   {
        
        let parameters: [String: Any] = [
            "Token": SessionInformation.Token,
            "DealerCode": SessionInformation.Bayi_?.id,
            "TofasCompany": SessionInformation.TofasCompany,
            "Data": [
                "Name": "Basket_GetList_ShippingType",
                "Parameters": [
                    "DealerId": SessionInformation.Bayi_?.id
                ]
            ]
        ]
        
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        
        Alamofire.request("https://b2b.opar.com/Api/Mobile/Basket_GetList_ShippingType", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .response(
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    // You are now running on the concurrent `queue` you created earlier.
                    switch response.result {
                        
                    case .success(let JSON):
                        
                        let response = JSON as! NSDictionary
                        let data = response.object(forKey: "Data") as? [[String:Any]]
                        var result = [ShippingInfo]()
                        
                        if(data != nil) {
                            
                            for item in data! {
                                var info = ShippingInfo()
                                info.id = item["Id"] as! Int
                                info.text = item["Name"] as! String?
                                result.append(info
                                )
                            }
                        }
                        
                        DispatchQueue.main.async {
                            completionHandler( result)
                        }
                        
                        
                        break
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        DispatchQueue.main.async {
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                                (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }                        }                    }
                    // To update anything on the main thread, just jump back on like so.
                    
            }
        )
    }
}
