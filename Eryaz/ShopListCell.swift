//
//  ShopListCell.swift
//  Eryaz
//
//  Created by Ferit Özcan on 28.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import DropDown

class ShopListCell: DropDownCell {
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!

    @IBOutlet weak var countTF: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
