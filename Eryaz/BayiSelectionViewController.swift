//
//  SplashViewController.swift
//  Eryaz
//
//  Created by Ferit Özcan on 28.06.2018.
//  Copyright © 2018 EryazSoftware. All rights reserved.
//

import UIKit
import RevealingSplashView

class BayiSelectionViewController:UIViewController{
    var isContact=false
    @IBOutlet weak var bayiView: UIView!
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    var vc : OparViewController!
    var repo=LoginRepository()
    @IBOutlet weak var tableView: UITableView!
    var bayiList=[BayiSelectionData]()
    var contactController:ContactViewController!
    override func viewDidLoad() {
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds

        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(blurEffectView, at: 0)
        
        modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        self.showAnimate()
        
        bayiView.layer.cornerRadius = 20
//        bayiView.clipsToBounds = true
//        bayiView.frame = bayiView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))

     
    }
    override func viewWillAppear(_ animated: Bool) {
        activityIndicator.hidesWhenStopped = true
       
        
        activityIndicator.startAnimating()

       /* if(isContact){
            repo.GetBayiListWithAdress(completionHandler:{ response in
                self.bayiList = response
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.activityIndicator.stopAnimating()

                }
            })
        }
        else{
            repo.GetBayiList(completionHandler:{ response in
                self.bayiList = response
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.activityIndicator.stopAnimating()

                }
            })
        }*/
        repo.GetBayiList(completionHandler:{ response in
            self.bayiList = response.data
                      DispatchQueue.main.async {
                          self.tableView.reloadData()
                          self.activityIndicator.stopAnimating()

                      }
                  })
        
    }
    func showAnimate()
    {
       
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {

        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
}

extension BayiSelectionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:BayiSelectionTableCell = self.tableView.dequeueReusableCell(withIdentifier: "bayiSelectionTableCell") as! BayiSelectionTableCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = Color.textColor.cgColor
        
        cell.shortNameLabel.text = self.bayiList[indexPath.row].shortName
        cell.bayiKoduLabel.text = self.bayiList[indexPath.row].code
        
        cell.clipsToBounds = true
        
        cell.backgroundColor = DesignData.weakBlue
        return cell
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.bayiList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 50.0;//Choose your custom row height
    }
    
}

extension BayiSelectionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isContact){
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            SessionInformation.selectedContactBayi = self.bayiList[indexPath.row]
            self.contactController.reloadTable()
            self.removeAnimate()

        }
        else{
            SessionInformation.Bayi_ = self.bayiList[indexPath.row]
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "leftMenuViewController") as! MenuController
           // controller.tableView.reloadData()
            
            let controller2 = self.storyboard!.instantiateViewController(withIdentifier: "oparViewController") as! OparViewController
            
            if(SessionInformation.TofasCompany == "OPAR"){
                vc.setPopularLisT()

            }
            
    
            
            self.removeAnimate()
        }

        
    }
}
